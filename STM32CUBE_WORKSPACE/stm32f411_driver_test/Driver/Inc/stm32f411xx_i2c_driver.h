/* stm32f411xx_i2c_driver.h */

#ifndef I2C_DRIVER_H_
#define I2C_DRIVER_H_

#include "stm32f411xx.h"
#include "stm32f411xx_rcc_driver.h"

/*
 * Configuration structure for I2Cx peripheral
 */

typedef struct
{
	uint32_t I2C_SCLSpeed;
	uint8_t	 I2C_DeviceAddress;
	uint8_t	 I2C_ACKControl;
	uint16_t I2C_FMDutyCycle;
} I2C_Config_t;

/*
 * Handle structure for I2Cx peripheral
 */

typedef struct
{
	I2C_RegDef_t *pI2Cx;
	I2C_Config_t  I2C_Config;
	uint8_t		  *pTxBuffer;		/* !< To store the app. Tx buffer address >! */
	uint8_t		  *pRxBuffer;		/* !< To store the app. Rx buffer address >! */
	uint32_t	  TxLen;			/* !< To store Tx Len					  >! */
	uint32_t	  RxLen;			/* !< To store Rx Len					  >! */
	uint8_t		  TxRxState;		/* !< To store communication state		  >! */
	uint8_t		  DeviceAddr;		/* !< To store slave/device address		  >! */
	uint32_t	  RxSize;			/* !< To store Rx size					  >! */
	uint8_t		  StartRepeat;		/* !< To store repeated start value		  >! */

} I2C_Handle_t;


/*
 * @I2C application states
 */
#define I2C_READY				0
#define I2C_BUSY_IN_RX			1
#define I2C_BUSY_IN_TX			2


/*
 * @I2C_SCLSpeed
 */

#define I2C_SCL_SPEED_SM		100000
#define I2C_SCL_SPEED_FM4K		400000
#define I2C_SCL_SPEED_FM2K		200000

/*
 * @ ACK control
 */

#define I2C_ACK_ENABLE			1
#define I2C_ACK_DISABLE			0


/*
 * @I2C_DutyCycle
 */

#define I2C_FM_DUTY_2			0
#define I2C_FM_DUTY_16_9		1


/*
 * @I2C_Flag
 */

/* @SR1 */
#define I2C_FLAG_SB 					(1 << I2C_SR1_SB)
#define I2C_FLAG_ADDR 					(1 << I2C_SR1_ADDR)
#define I2C_FLAG_BTF 					(1 << I2C_SR1_BTF)
#define I2C_FLAG_ADD10 					(1 << I2C_SR1_ADD10)
#define I2C_FLAG_STOPF 					(1 << I2C_SR1_STOPF)
#define I2C_FLAG_RxNE 					(1 << I2C_SR1_RxNE)
#define I2C_FLAG_TxE 					(1 << I2C_SR1_TxE)
#define I2C_FLAG_BERR 					(1 << I2C_SR1_BERR)
#define I2C_FLAG_ARLO 					(1 << I2C_SR1_ARLO)
#define I2C_FLAG_AF 					(1 << I2C_SR1_AF)
#define I2C_FLAG_OVR 					(1 << I2C_SR1_OVR)
#define I2C_FLAG_PECERR 				(1 << I2C_SR1_PECERR)
#define I2C_FLAG_TIMEOUT 				(1 << I2C_SR1_TIMEOUT)

/* @SR2 */
#define I2C_FLAG_MSL 					(1 << I2C_SR2_MSL)
#define I2C_FLAG_BUSY 					(1 << I2C_SR2_BUSY)
#define I2C_FLAG_TRA 					(1 << I2C_SR2_TRA)
#define I2C_FLAG_GENCALL 				(1 << I2C_SR2_GENCALL)
#define I2C_FLAG_SMBDEFAULT 			(1 << I2C_SR2_SMBDEFAULT)
#define I2C_FLAG_SMBHOST 				(1 << I2C_SR2_SMBHOST)
#define I2C_FLAG_DUALF 					(1 << I2C_SR2_DUALF)

#define NOT_REPEATED_START				0
#define REPEATED_START 					1

/* @I2C event macro */
#define I2C_EV_RX_CMPLT					0
#define I2C_EV_TX_CMPLT					1
#define I2C_EV_STOPF					2
#define I2C_ERROR_BERR  				3
#define I2C_ERROR_ARLO 				    4
#define I2C_ERROR_AF    				5
#define I2C_ERROR_OVR   				6
#define I2C_ERROR_TIMEOUT 				7
#define I2C_EV_DATA_RCV					8
#define I2C_EV_DATA_REQ					9

/* **************************************************************************************************
 *														APIs supported by this driver
					For more information about the APIs check the function definitions
 **************************************************************************************************** */

/*
 * Peripheral Clock Setup
*/
void I2C_PeriClockControl (I2C_RegDef_t *pI2Cx, uint8_t EnorDi);

/*
 * I2C init - deinit
*/
void I2C_Init (I2C_Handle_t *pI2CHandle);
void I2C_DeInit (I2C_RegDef_t *pI2Cx);


/*
 * Data Send and Receive
 */

void I2C_MasterSendData (I2C_Handle_t *pI2CHandle, uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr);
void I2C_MasterReceiveData (I2C_Handle_t *pI2CHandle, uint8_t *pRxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr);

/*
 * IRQ Configuration and ISR handling
 */

void I2C_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi);
void I2C_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority);
void I2C_EV_IRQHandling (I2C_Handle_t *pI2CHandle);
void I2C_ER_IRQHandling (I2C_Handle_t *pI2CHandle);



uint8_t I2C_GetFlagStatus (I2C_RegDef_t *pI2Cx, uint32_t FlagName, uint8_t RegNumber);
void I2C_SlaveEnableDisableCallbackEvents (I2C_RegDef_t *pI2Cx, uint8_t EnorDi);

/*
 * Enalbe or Disable I2C peripherals
 */
void I2C_PeripheralControl (I2C_RegDef_t *pI2Cx, uint8_t EnorDi);


/*
 * application call back
 */
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle, uint8_t AppEvent);





/*
 * Data Send and Receive with interrupt
 */
uint8_t I2C_MasterSendDataIT (I2C_Handle_t *pI2CHandle, uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr);
uint8_t I2C_MasterReceiveDataIT (I2C_Handle_t *pI2CHandle, uint8_t *pRxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr);

/*
 * close send and receive data
 */
void I2C_CloseSendData (I2C_Handle_t *pI2CHandle);
void I2C_CloseReceiveData (I2C_Handle_t *pI2CHandle);


/*
 * Data Send and Receive (slave mode)
 */
void I2C_SlaveSendData (I2C_RegDef_t *pI2C, uint8_t data);
uint8_t I2C_SlaveReceiveData (I2C_RegDef_t *pI2C);





#endif
















