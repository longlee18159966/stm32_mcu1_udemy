/* stm32f411xx_spi_driver.h */

#ifndef SPI_DRIVER_H_
#define SPI_DRIVER_H_

#include "stm32f411xx.h"
#include "stm32f411xx_rcc_driver.h"




/*
 * This is configuration structure for SPIx peripheral
 */

typedef struct
{
	uint8_t SPI_DeviceMode;
	uint8_t SPI_BusConfig;
	uint8_t SPI_SclkSpeed;
	uint8_t SPI_DFF;				/* Data Frame Format */
	uint8_t SPI_CPOL;
	uint8_t SPI_CPHA;
	uint8_t SPI_SSM;				/* SS Manegerment */
} SPI_Config_t;


/*
 * Handle structure for SPIx peripheral
 */

typedef struct
{
	SPI_RegDef_t	*pSPIx;			/* !< This holding the base addressof SPIx peripheral >! */
	SPI_Config_t	SPIConfig;
	uint8_t			*pTxBuffer;		/* !< To store the app. Tx buffer address >! */
	uint8_t			*pRxBuffer;		/* !< To store the app. Rx buffer address >! */
	uint32_t		TxLen;			/* !< To store Tx len >! */
	uint32_t		RxLen;			/* !< To store Rx len >! */
	uint8_t			TxState;		/* !< To store Tx state >! */
	uint8_t			RxState;		/* !< To store Rx state >! */
} SPI_Handle_t;

/*
 * SPI application state
 */

#define SPI_READY						0
#define SPI_BUSY_IN_RX					1
#define SPI_BUSY_IN_TX					2

/*
 * Possible SPI Application events
 */

#define SPI_EVENT_TX_CMPLT				1
#define SPI_EVENT_RX_CMPLT				2
#define SPI_EVENT_OVR_ERR				3
#define SPI_EVENT_CRC_ERR				4

/*
 * @SPI_DeviceMoide
 */
#define SPI_DEVICEMODE_MASTER			1
#define SPI_DEVICEMODE_SLAVE			0

/*
 * @SPI_BusConfig
 */

#define SPI_BUS_CONFIG_FULL_DUPLEX		1
#define SPI_BUS_CONFIG_HALF_DUPLEX		2
#define SPI_BUS_CONFIG_SIMPLEX_RXONLY	3

/*
 * @SPI_SclkSpeed
 */

#define SPI_SCLK_SPEED_DIV_2			0
#define SPI_SCLK_SPEED_DIV_4			1
#define SPI_SCLK_SPEED_DIV_8			2
#define SPI_SCLK_SPEED_DIV_16			3
#define SPI_SCLK_SPEED_DIV_32			4
#define SPI_SCLK_SPEED_DIV_64			5
#define SPI_SCLK_SPEED_DIV_128			6
#define SPI_SCLK_SPEED_DIV_256			7

/*
 * @SPI_DFF
 */
#define SPI_DFF_8_BIT					0
#define SPI_DFF_16_BIT					1

/*
 * @CPOL
 */
#define SPI_CPOL_LOW					0
#define SPI_CPOL_HIGH					1

/*
 * @CPHA
 */
#define SPI_CPHA_LOW					0
#define SPI_CPHA_HIGH					1


/*
 * SPI_SSM
 */

#define SPI_SSM_EN						1
#define SPI_SSM_DI						0

/*
 * SPI_Flag
 */

#define SPI_FLAG_RXNE 					(1 << SPI_SR_RXEN)
#define	SPI_FLAG_TXE 					(1 << SPI_SR_TXE)
#define SPI_FLAG_CHSIDE					(1 << SPI_SR_CHSIDE)
#define SPI_FLAG_UDR					(1 << SPI_SR_UDR)
#define SPI_FLAG_CRCERR					(1 << SPI_SR_CRCERR)
#define SPI_FLAG_MODF					(1 << SPI_SR_MODF)
#define SPI_FLAG_OVR					(1 << SPI_SR_OVR)
#define SPI_FLAG_BSY					(1 << SPI_SR_BSY)
#define SPI_FLAG_FRE					(1 <<SPI_SR_FRE)






























/* **************************************************************************************************
 *														APIs supported by this driver
					For more infomation about the APIs check the function definitions
 **************************************************************************************************** */

/*
 * Peripheral Clock Setup
*/
void SPI_PeriClockControl (SPI_RegDef_t *pSPIx, uint8_t EnorDi);

/*
 * SPI init - deinit
*/
void SPI_Init (SPI_Handle_t *pSPIHandle);
void SPI_DeInit (SPI_RegDef_t *pSPIx);


/*
 * Data Send and Receive
 */

void SPI_SendData (SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t Len);
void SPI_ReceiveData (SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t Len);

uint8_t SPI_SendDataIT (SPI_Handle_t *pSPIHandle, uint8_t *pTxBuffer, uint32_t Len);
uint8_t SPI_ReceiveDataIT (SPI_Handle_t  *pSPIHandle, uint8_t *pRxBuffer, uint32_t Len);

/*
 * IRQ Configuration and ISR handling
 */

void SPI_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi);
void SPI_IRQHandling (SPI_Handle_t *pHandle);
void SPI_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority);



uint8_t SPI_GetFlagStatus (SPI_RegDef_t *pSPIx, uint32_t FlagName);
void SPI_Polling (SPI_RegDef_t *pSPIx, uint32_t FlagName, uint8_t FlagStatus);

/*
 * Enalbe or Disable SPI peripherals
 */
void SPI_PeripheralControl (SPI_RegDef_t *pSPIx, uint8_t EnorDi);

/*
 * Configuration NSS pin
 */

void SPI_NSSConfiguration (SPI_RegDef_t *pSPIx, uint8_t EnorDI, uint8_t NssPinState);

void SPI_ClearOVRFlag (SPI_RegDef_t *pSPIx);
void SPI_CloseTransmission (SPI_Handle_t *pSPIHandle);
void SPI_CloseReception (SPI_Handle_t *pSPIHandle);

/*
 * application call back
 */
void SPI_ApplicationEventCallback(SPI_Handle_t *pSPIHandle, uint8_t AppEvent);


#endif
