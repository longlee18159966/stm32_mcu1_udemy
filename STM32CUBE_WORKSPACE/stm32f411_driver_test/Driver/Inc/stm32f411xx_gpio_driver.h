
/* stm32f411xx_gpio_driver.h */

#ifndef GPIO_DRIVER_H_
#define GPIO_DRIVER_H_

#include "stm32f411xx.h"

/*
 * This is a Configuration structure for a GPIO pin
*/

typedef struct
{
	uint8_t	GPIO_PinNumber;								/* !< possible value from @GPIO_PIN_NUMBER >! */
	uint8_t	GPIO_PinMode;								/* !< possible value from @GPIO_PIN_MODES >!  */
	uint8_t	GPIO_PinSpeed;								/* !< possible value from @GPIO_PIN_SPEEDS >! */
	uint8_t	GPIO_PinPuPdControl;						/* !< possible value from @GPIO_PIN_PUPD >!	  */
	uint8_t	GPIO_PinOPType;								/* !< possible value from @GPIO_PIN_OP_TYPE >!*/
	uint8_t	GPIO_PinAltFuncMode;						/* !< possible value from @GPIO_PIN_ALTFUNC >!*/

} GPIO_PinConfig_t;

/*
 * This is Handle structure for a GPIO pin
*/

typedef struct
{

	GPIO_RegDef_t *pGPIOx;								/* This holds the base address of GPIO port to which the pin belong */
	GPIO_PinConfig_t GPIO_PinConfig;					/* This holds GPIO pin configuration setting */

} GPIO_Handle_t;

/*
 * @GPIO_PIN_NUMBER
 * GPIO pin number
 */

#define GPIO_PIN_NO0				0
#define GPIO_PIN_NO1				1
#define GPIO_PIN_NO2				2
#define GPIO_PIN_NO3				3
#define GPIO_PIN_NO4				4
#define GPIO_PIN_NO5				5
#define GPIO_PIN_NO6				6
#define GPIO_PIN_NO7				7
#define GPIO_PIN_NO8				8
#define GPIO_PIN_NO9				9
#define GPIO_PIN_NO10				10
#define GPIO_PIN_NO11				11
#define GPIO_PIN_NO12				12
#define GPIO_PIN_NO13				13
#define GPIO_PIN_NO14				14
#define GPIO_PIN_NO15				15


/*
 * @GPIO_PIN_MODES
 * GPIO pin possible modes
 */

#define GPIO_MODE_INPUT				0
#define GPIO_MODE_OUTPUT			1
#define GPIO_MODE_ALTFUNC			2
#define GPIO_MODE_ANALOG			3
#define GPIO_MODE_IT_FT				4
#define GPIO_MODE_IT_RT				5
#define GPIO_MODE_IT_RFT			6

/*
 * @GPIO_PIN_OP_TYPE
 * GPIO pin output types
 */

#define GPIO_OUTPUT_TYPE_PP			0
#define GPIO_OUTPUT_TYPE_OD			1

/*
 * @GPIO_PIN_SPEEDS
 * GPIO output seeds
 */

#define GPIO_OUTPUT_SPEED_LOW		0
#define GPIO_OUTPUT_SPEED_MEDIUM	1
#define GPIO_OUTPUT_SPEED_FAST		2
#define GPIO_OUTPUT_SPEED_HIGH		3

/*
 * @GPIO_PIN_PUPD
 * GPIO pull-up/pull-down
 */

#define GPIO_NO_PU_PD				0
#define GPIO_PIN_PU					1
#define GPIO_PIN_PD					2



/* **************************************************************************************************
 *														APIs supported by this driver
					For more infomation about the APIs check the function definitions
 **************************************************************************************************** */

/*
 * Peripheral Clock Setup
*/

void GPIO_PeriClockControl (GPIO_RegDef_t *pGPIOx, uint8_t EnorDi);

/*
 * GPIO init - deinit
*/
void GPIO_Init (GPIO_Handle_t *pGPIOHandle);
void GPIO_DeInit (GPIO_RegDef_t *pGPIOx);

/*
 * data read - write
*/

uint8_t  GPIO_ReadFromInputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);
uint16_t GPIO_ReadFromInputPort (GPIO_RegDef_t *pGPIOx);

void GPIO_WriteToOutputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t	Value);
void GPIO_WriteToOutputPort (GPIO_RegDef_t *pGPIOx, uint16_t	Value);
void GPIO_ToggleOutputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);

/*
 * IRQ configuration and ISR handling
*/

void GPIO_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi);
void GPIO_IRQHandling (uint8_t PinNumber);
void GPIO_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority);



#endif
