
/* stm32f411xx.h */

#ifndef INC_STM32F411XX_H_
#define INC_STM32F411XX_H_

#include <stddef.h>
#include <stdint.h>


#define __vo volatile

/* ********************* Processor Specific Detail ************************************************ */

/*
 * ARM Cortex Mx Processor NVIC ISERx Register Address
 */

#define NVIC_ISER0				((__vo uint32_t *) 0xE000E100)
#define NVIC_ISER1				((__vo uint32_t *) 0xE000E104)
#define NVIC_ISER2				((__vo uint32_t *) 0xE000E108)
#define NVIC_ISER3				((__vo uint32_t *) 0xE000E10C)

/*
 * ARM Cortex Mx Processor NVIC ICERx Register Address
 */

#define NVIC_ICER0				((__vo uint32_t *) 0XE000E180)
#define NVIC_ICER1				((__vo uint32_t *) 0XE000E184)
#define NVIC_ICER2				((__vo uint32_t *) 0XE000E188)
#define NVIC_ICER3				((__vo uint32_t *) 0XE000E18C)

/*
 * ARM Cortex Mx Processor NVIC Priority Register Address Calculation
 */

#define NVIC_PR_BASE_ADDRESS	((__vo uint32_t *) 0xE000E400)

#define NO_PR_BIT_IMPLEMENTED	4

/*
 * base addresses of flash and SRAM memories
 */

#define FLASH_BASEADDR			0x08000000U			/* base address of FLASH memory */
#define SRAM1_BASEADDR			0x20000000U			/* base address of SRAM1 memory */
#define ROM						0x1FFF0000U			/* base address of ROM memory 	*/
#define SRAM 					SRAM1_BASEADDR		/* base address of SRAM memory 	*/

/*
 * AHBx and APBx Bus Peripheral base addresses
*/

#define PERIPH_BASE				0x40000000U
#define APB1PERIPH_BASE			PERIPH_BASE
#define APB2PERIPH_BASE			0x40010000U
#define AHB1PERIPH_BASE			0x40020000U
#define AHB2PERIPH_BASE			0x50000000U

/*
 * Base addresses of peripherals which are hanging on AHB1 bus
*/

#define GPIOA_BASEADDR			(AHB1PERIPH_BASE + 0x0000U)
#define GPIOB_BASEADDR			(AHB1PERIPH_BASE + 0x0400U)
#define GPIOC_BASEADDR			(AHB1PERIPH_BASE + 0x0800U)
#define GPIOD_BASEADDR			(AHB1PERIPH_BASE + 0x0C00U)
#define GPIOE_BASEADDR			(AHB1PERIPH_BASE + 0x1000U)
#define GPIOH_BASEADDR			(AHB1PERIPH_BASE + 0x1C00U)
#define RCC_BASEADDR			(AHB1PERIPH_BASE + 0x3800U)

/*
 * Base addresses of peripherals which are hanging on APB1 bus
*/

#define I2C1_BASEADDR			(APB1PERIPH_BASE + 0X5400U)
#define I2C2_BASEADDR			(APB1PERIPH_BASE + 0X5800U)
#define I2C3_BASEADDR			(APB1PERIPH_BASE + 0X5c00U)
#define SPI2_BASEADDR			(APB1PERIPH_BASE + 0X3800U)
#define SPI3_BASEADDR			(APB1PERIPH_BASE + 0X3C00U)
#define USART2_BASEADDR			(APB1PERIPH_BASE + 0X4400U)

/*
 * Base addresses of peripherals which are hanging on APB2 bus
*/

#define USART1_BASEADDR		 	(APB2PERIPH_BASE + 0X1000U)
#define USART6_BASEADDR		 	(APB2PERIPH_BASE + 0X1400U)
#define SPI1_BASEADDR		 	(APB2PERIPH_BASE + 0X3000U)
#define SPI4_BASEADDR		 	(APB2PERIPH_BASE + 0X3400U)
#define SPI5_BASEADDR		 	(APB2PERIPH_BASE + 0X5000U)
#define SYSCFG_BASEADDR		 	(APB2PERIPH_BASE + 0X3800U)
#define EXTI_BASEADDR		 	(APB2PERIPH_BASE + 0X3C00U)



/* *************************************************** peripheral register definition strctures ***************************************************** */

/*
 * Note: Registers of a peripheral are specific to MCU
 * Please check your Device RM
*/

/*
 * peripheral definition structure for GPIO
 */

typedef struct
{
	__vo uint32_t MODER;																/* GPIO port mode register, 				Address offset: 0x00 */
	__vo uint32_t OTYPER;																/* GPIO port output type register , 		Address offset: 0x04 */
	__vo uint32_t OSPEEDR;																/* GPIO port output speed register, 		Address offset: 0x08 */
	__vo uint32_t PUPDR;																/* GPIO port pull-up/pull-down register, 	Address offset: 0x0C */
	__vo uint32_t IDR;																	/* GPIO port input data register, 			Address offset: 0x10 */
	__vo uint32_t ODR;																	/* GPIO port output data register, 			Address offset: 0x14 */
	__vo uint32_t BSRR;																	/* GPIO port bit set/reset register, 		Address offset: 0x18 */
	__vo uint32_t LCKR;																	/* GPIO port configuration lock register,	Address offset: 0x1C */
	__vo uint32_t AFRL;																	/* GPIO alternate function low register, 	Address offset: 0x20 */
	__vo uint32_t AFRH;																	/* GPIO alternate function high register,	Address offset: 0x24 */

} GPIO_RegDef_t;

/*
 * peripheral definition structure for RCC
 */

typedef struct
{
	__vo uint32_t	CR;
	__vo uint32_t	PLLCFGR;
	__vo uint32_t	CFGR;
	__vo uint32_t	CIR;
	__vo uint32_t	AHB1RSTR;
	__vo uint32_t	AHB2RSTR;

	__vo uint32_t	Reserved0;
	__vo uint32_t	Reserved01;

	__vo uint32_t	APB1RSTR;
	__vo uint32_t	APB2RSTR;

	__vo uint32_t	Reserved1;
	__vo uint32_t	Reserved2;

	__vo uint32_t	AHB1ENR;
	__vo uint32_t	AHB2ENR;

	__vo uint32_t	Reserved3;
	__vo uint32_t	Reserved4;

	__vo uint32_t	APB1ENR;
	__vo uint32_t	APB2ENR;

	__vo uint32_t	Reserved5;
	__vo uint32_t	Reserved6;

	__vo uint32_t	AHB1LPENR;
	__vo uint32_t	AHB2LPENR;

	__vo uint32_t	Reserved7;
	__vo uint32_t	Reserved8;

	__vo uint32_t	APB1LPENR;
	__vo uint32_t	APB2LPENR;

	__vo uint32_t	Reserved9;
	__vo uint32_t	Reserved10;

	__vo uint32_t	BDCR;
	__vo uint32_t	CSR;

	__vo uint32_t	Reserved11;
	__vo uint32_t	Reserved12;

	__vo uint32_t	SSCGR;
	__vo uint32_t	PLLI2SCFGR;
	__vo uint32_t	DCKCFGR;

} RCC_RegDef_t;

/*
 * peripheral definition structure for EXTI
 */

typedef struct
{
	__vo uint32_t	IMR;
	__vo uint32_t	EMR;
	__vo uint32_t	RTSR;
	__vo uint32_t	FTSR;
	__vo uint32_t	SWIER;
	__vo uint32_t	PR;
} EXTI_RegDef_t;

/*
 * peripheral definition structure for SYSCFG
 */

typedef struct
{
	__vo uint32_t	MEMRMP;
	__vo uint32_t	PMC;
	__vo uint32_t	EXTICR[4];
	uint32_t		RESERVED1[2];
	__vo uint32_t	CMPCR;
} SYSCFG_RegDef_t;


/*
 * peripheral definitions
*/

#define GPIOA						((GPIO_RegDef_t *)GPIOA_BASEADDR)
#define GPIOB						((GPIO_RegDef_t *)GPIOB_BASEADDR)
#define GPIOC						((GPIO_RegDef_t *)GPIOC_BASEADDR)
#define GPIOD						((GPIO_RegDef_t *)GPIOD_BASEADDR)
#define GPIOE						((GPIO_RegDef_t *)GPIOE_BASEADDR)
#define GPIOH						((GPIO_RegDef_t *)GPIOH_BASEADDR)
#define RCC							((RCC_RegDef_t *) RCC_BASEADDR)

#define EXTI						((EXTI_RegDef_t *) EXTI_BASEADDR)
#define SYSCFG						((SYSCFG_RegDef_t *)SYSCFG_BASEADDR)

/* ****************************************************************************** */

/*
 * Clock Enable Macros for GPIOx peripherals
*/

#define GPIOA_PCLK_EN()				( RCC->AHB1ENR |= (1 << 0) )
#define GPIOB_PCLK_EN()				( RCC->AHB1ENR |= (1 << 1) )
#define GPIOC_PCLK_EN()				( RCC->AHB1ENR |= (1 << 2) )
#define GPIOD_PCLK_EN()				( RCC->AHB1ENR |= (1 << 3) )
#define GPIOE_PCLK_EN()				( RCC->AHB1ENR |= (1 << 4) )
#define GPIOH_PCLK_EN()				( RCC->AHB1ENR |= (1 << 7) )

/*
 * Clock Enable Macros for I2Cx peripherals
*/

#define I2C1_CLK_EN()				( RCC->APB1ENR |= (1 << 21) )
#define I2C2_CLK_EN()				( RCC->APB1ENR |= (1 << 22) )
#define I2C3_CLK_EN()				( RCC->APB1ENR |= (1 << 23) )



/*
 * Clock Enable Macros for SPIx peripherals
*/

#define SPI1_CLK_EN()				( RCC->APB2ENR |= (1 << 12) )
#define SPI2_CLK_EN()				( RCC->APB1ENR |= (1 << 14) )
#define SPI3_CLK_EN()				( RCC->APB1ENR |= (1 << 15) )
#define SPI4_CLK_EN()				( RCC->APB2ENR |= (1 << 13) )
#define SPI5_CLK_EN()				( RCC->APB2ENR |= (1 << 20) )

/*
 * Clock Enable Macros for USARTx peripherals
*/

#define USART1_CLK_EN()				( RCC->APB2ENR |= (1 << 4) )
#define USART2_CLK_EN()				( RCC->APB1ENR |= (1 << 17 ) )
#define USART6_CLK_EN()				( RCC->APB2ENR |= (1 << 5 ) )

/*
 * Clock Enable Macros for SYSCFG peripherals
*/

#define SYSCFG_CLK_EN()				( RCC->APB2ENR |= (1 << 14) )


/* *************************************************************************************** */


/*
 * Clock Disable Macros for GPIOx peripherals
*/

#define GPIOA_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 0) )
#define GPIOB_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 1) )
#define GPIOC_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 2) )
#define GPIOD_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 3) )
#define GPIOE_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 4) )
#define GPIOH_PCLK_DI()				( RCC->AHB1ENR &= ~(1 << 7) )

/*
 * Macros to reset GPIOx
 */

#define GPIOA_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 0) ); RCC->AHB1RSTR &= ~(1 << 0);} while (0)
#define GPIOB_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 1) ); RCC->AHB1RSTR &= ~(1 << 1);} while (0)
#define GPIOC_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 2) ); RCC->AHB1RSTR &= ~(1 << 2);} while (0)
#define GPIOD_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 3) ); RCC->AHB1RSTR &= ~(1 << 3);} while (0)
#define GPIOE_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 4) ); RCC->AHB1RSTR &= ~(1 << 4);} while (0)
#define GPIOH_REG_RESET()			do{( RCC->AHB1RSTR |= (1 << 7) ); RCC->AHB1RSTR &= ~(1 << 7);} while (0)

/*
 * Clock Disable Macros for I2Cx peripherals
*/

#define I2C1_CLK_DI()				( RCC->APB1ENR &= ~(1 << 21) )
#define I2C2_CLK_DI()				( RCC->APB1ENR &= ~(1 << 22) )
#define I2C3_CLK_DI()				( RCC->APB1ENR &= ~(1 << 23) )



/*
 * Clock Disable Macros for SPIx peripherals
*/

#define SPI1_CLK_DI()				( RCC->APB2ENR &= ~(1 << 12) )
#define SPI2_CLK_DI()				( RCC->APB1ENR &= ~(1 << 14) )
#define SPI3_CLK_DI()				( RCC->APB1ENR &= ~(1 << 15) )
#define SPI4_CLK_DI()				( RCC->APB2ENR &= ~(1 << 13) )
#define SPI5_CLK_DI()				( RCC->APB2ENR &= ~(1 << 20) )

/*
 * Clock Disable Macros for USARTx peripherals
*/

#define USART1_CLK_DI()				( RCC->APB2ENR &= ~(1 << 17) )
#define USART2_CLK_DI()				( RCC->APB1ENR &= ~(1 << 4 ) )
#define USART6_CLK_DI()				( RCC->APB2ENR &= ~(1 << 5 ) )

/*
 * Clock Disable Macros for SYSCFG peripherals
*/

#define SYSCFG_CLK_DI()				( RCC->APB2ENR &= ~(1 << 14) )


/* *************************************************************************************** */



#define GPIO_BASEADRRESS_TO_CODE(x) ((x == GPIOA) ? 0 :\
									 (x == GPIOB) ? 1 :\
									 (x == GPIOC) ? 2: \
									 (x == GPIOD) ? 3: \
									 (x == GPIOE) ? 4: \
									 (x == GPIOH) ? 7: 0 )


/*
 * define IRQ EXTI
 */

#define IRQ_NO_EXTI0					6
#define IRQ_NO_EXTI1					7
#define IRQ_NO_EXTI2					8
#define IRQ_NO_EXTI3					9
#define IRQ_NO_EXTI4					10
#define IRQ_NO_EXTI9_5					23
#define IRQ_NO_EXTI10_15			    40

/*
 * #define IRQ SPI
 */

#define IRQ_NO_SPI1						35
#define IRQ_NO_SPI2						36
#define IRQ_NO_SPI3						51
#define IRQ_NO_SPI4						84
#define IRQ_NO_SPI5						85

/*
 * define IRQ I2C
 */
#define IRQ_NO_I2C1_EV					31
#define IRQ_NO_I2C1_ER					32
#define IRQ_NO_I2C2_EV					33
#define IRQ_NO_I2C2_ER					34
#define IRQ_NO_I2C3_EV					72
#define IRQ_NO_I2C3_ER					73

/*
 * define IRQ USART
 */
#define IRQ_NO_USART1					37
#define IRQ_NO_USART2					38
#define IRQ_NO_USART6					71



/* *************************************************************************************** */



/*
 * macros for all priority possible
 */

#define NVIC_IRQ_PRI0					0
#define NVIC_IRQ_PRI1					1
#define NVIC_IRQ_PRI2					2
#define NVIC_IRQ_PRI3					3
#define NVIC_IRQ_PRI4					4
#define NVIC_IRQ_PRI5					5
#define NVIC_IRQ_PRI6					6
#define NVIC_IRQ_PRI7					7
#define NVIC_IRQ_PRI8					8
#define NVIC_IRQ_PRI9					9
#define NVIC_IRQ_PRI10					10
#define NVIC_IRQ_PRI11					11
#define NVIC_IRQ_PRI12					12
#define NVIC_IRQ_PRI13					13
#define NVIC_IRQ_PRI14					14
#define NVIC_IRQ_PRI15					15



 /* some generic macros */

 #define ENABLE 					1
 #define DISABLE					0
 #define SET						ENABLE
 #define RESET						DISABLE
 #define GPIO_PIN_SET 				SET
 #define GPIO_PIN_RESET				RESET

/* *********************************************************************************************** */
/* ************************************ SPI DRIVER *********************************************** */

/*
 * This is SPI register definition structure
 */

typedef struct
{
	__vo uint32_t CR1;						/* !< SPI Control Register 1 	  	 >! */
	__vo uint32_t CR2;						/* !< SPI Control Register 2 	  	 >! */
	__vo uint32_t SR;						/* !< SPI Status Register 	 	  	 >! */
	__vo uint32_t DR;						/* !< SPI Data Register		 	  	 >! */
	__vo uint32_t CRCPR;					/* !< SPI CRC polynomial register 	 >! */
	__vo uint32_t RXCRCR;					/* !< SPI RX CRC register		  	 >! */
	__vo uint32_t TXCRCR;					/* !< SPI TX CRC register		  	 >! */
	__vo uint32_t I2SCFGR;					/* !< SPI I2S configuration register >! */
	__vo uint32_t I2SPR;					/* !< SPT I2S prescaler register	 >! */
} SPI_RegDef_t;

/*
 * Macro define SPIx
 */

#define SPI1						(SPI_RegDef_t *) (SPI1_BASEADDR)
#define SPI2						(SPI_RegDef_t *) (SPI2_BASEADDR)
#define SPI3						(SPI_RegDef_t *) (SPI3_BASEADDR)
#define SPI4						(SPI_RegDef_t *) (SPI4_BASEADDR)
#define SPI5						(SPI_RegDef_t *) (SPI5_BASEADDR)

/*
 * Macro to RESET SPIx
 */
#define SPI1_REG_RESET()			do{( RCC->APB2RSTR |= (1 << 12) ); RCC->APB2RSTR &= ~(1 << 12);} while (0)
#define SPI2_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 14) ); RCC->APB1RSTR &= ~(1 << 14);} while (0)
#define SPI3_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 15) ); RCC->APB1RSTR &= ~(1 << 15);} while (0)
#define SPI4_REG_RESET()			do{( RCC->APB2RSTR |= (1 << 13) ); RCC->APB2RSTR &= ~(1 << 13);} while (0)
#define SPI5_REG_RESET()			do{( RCC->APB2RSTR |= (1 << 20) ); RCC->APB2RSTR &= ~(1 << 20);} while (0)



/*
 * Macro for bit position for SPI
 */

/*
 * SPI_CR1 register
 */

#define SPI_CR1_CPHA				0
#define SPI_CR1_CPOL				1
#define SPI_CR1_MSTR				2
#define SPI_CR1_BR					3
#define SPI_CR1_SPE					6
#define SPI_CR1_LSBFIRST			7
#define SPI_CR1_SSI					8
#define SPI_CR1_SSM					9
#define SPI_CR1_RXONLY				10
#define SPI_CR1_DFF					11
#define SPI_CR1_CRCNEXT				12
#define SPI_CR1_CRCEN				13
#define SPI_CR1_BIDIOE				14
#define SPI_CR1_BIDIMODE			15

/*
 * SPI_CR2 register
 */

#define SPI_CR2_RXDMAEN				0
#define SPI_CR2_TXDMAEN				1
#define SPI_CR2_SSOE				2
#define SPI_CR2_FRF					4
#define SPI_CR2_ERRIE				5
#define SPI_CR2_RXEIE				6
#define SPI_CR2_TXEIE				7

/*
 * SPI_SR register
 */

#define SPI_SR_RXEN					0
#define SPI_SR_TXE					1
#define SPI_SR_CHSIDE				2
#define SPI_SR_UDR					3
#define SPI_SR_CRCERR				4
#define SPI_SR_MODF					5
#define SPI_SR_OVR					6
#define SPI_SR_BSY					7
#define SPI_SR_FRE					8


/*
 * FLAG STATUS
 */

#define FLAG_SET					1
#define FLAG_RESET					0

/*
 * GPIO state
 */

#define INPUT						0
#define OUTPUT						1


/*
 * This is I2C register definition structure
 */

typedef struct
{
	__vo uint32_t CR1;						/* !< I2C Control Register 1 	  	 >! */
	__vo uint32_t CR2;						/* !< I2C Control Register 2 	  	 >! */
	__vo uint32_t OAR1;						/* !< I2C Own Address Register 1 	 >! */
	__vo uint32_t OAR2;						/* !< I2C Own Address Register 2 	 >! */
	__vo uint8_t DR;						/* !< I2C Data Register  	  	 	 >! */
	__vo uint32_t SR1;						/* !< I2C Status Register 1 	  	 >! */
	__vo uint32_t SR2;						/* !< I2C Status Register 2 	  	 >! */
	__vo uint32_t CCR;						/* !< I2C Clock Control Register 1 	 >! */
	__vo uint32_t TRISE;					/* !< I2C TRISE Register 1 	  	 	 >! */
	__vo uint32_t FLTR;						/* !< I2C FLTR Register 1 	  	 	 >! */
} I2C_RegDef_t;

/*
 * Macro define I2Cx
 */

#define I2C1						(I2C_RegDef_t *) (I2C1_BASEADDR)
#define I2C2						(I2C_RegDef_t *) (I2C2_BASEADDR)
#define I2C3						(I2C_RegDef_t *) (I2C3_BASEADDR)

/*
 * Macro for bit position for I2C
 */




/*
 * @I2C_CR1 register
 */
#define I2C_CR1_PE 					0
#define I2C_CR1_SMBUS 				1
#define I2C_CR1_SMBTYPE 			3
#define I2C_CR1_ENARP 				4
#define I2C_CR1_ENPEC 				5
#define I2C_CR1_ENGC 				6
#define I2C_CR1_NOSTRETCH 			7
#define I2C_CR1_START 				8
#define I2C_CR1_STOP 				9
#define I2C_CR1_ACK 				10
#define I2C_CR1_POS 				11
#define I2C_CR1_PEC 				12
#define I2C_CR1_AlERT 				13
#define I2C_CR1_SWRST 				15

/*
 * @I2C_CR2 register
 */
#define I2C_CR2_FREQ0 				0
#define I2C_CR2_FREQ1 				1
#define I2C_CR2_FREQ2 				2
#define I2C_CR2_FREQ3 				3
#define I2C_CR2_FREQ4 				4
#define I2C_CR2_FREQ5 				5
#define I2C_CR2_ITERREN 			8
#define I2C_CR2_ITEVTEN 			9
#define I2C_CR2_ITBUFEN 			10
#define I2C_CR2_DMAEN 				11
#define I2C_CR2_LAST 				12

/*
 * @I2C_SR1 register
 */
#define I2C_SR1_SB 					0
#define I2C_SR1_ADDR 				1
#define I2C_SR1_BTF 				2
#define I2C_SR1_ADD10 				3
#define I2C_SR1_STOPF 				4
#define I2C_SR1_RxNE 				6
#define I2C_SR1_TxE 				7
#define I2C_SR1_BERR 				8
#define I2C_SR1_ARLO 				9
#define I2C_SR1_AF 					10
#define I2C_SR1_OVR 				11
#define I2C_SR1_PECERR 				12
#define I2C_SR1_TIMEOUT 			14
#define I2C_SR1_SMBALERT 			15


/*
 * @I2C_SR2 register
 */
#define I2C_SR2_MSL 				0
#define I2C_SR2_BUSY 				1
#define I2C_SR2_TRA 				2
#define I2C_SR2_GENCALL 			4
#define I2C_SR2_SMBDEFAULT 			5
#define I2C_SR2_SMBHOST 			6
#define I2C_SR2_DUALF 				7


/*
 * @I2C_CCR register
 */
#define I2C_CCR_0 					0
#define I2C_CCR_1					1
#define I2C_CCR_2 					2
#define I2C_CCR_3 					3
#define I2C_CCR_4 					4
#define I2C_CCR_5 					5
#define I2C_CCR_6 					6
#define I2C_CCR_7 					7
#define I2C_CCR_8 					8
#define I2C_CCR_9 					9
#define I2C_CCR_10 					10
#define I2C_CCR_11 					11
#define I2C_DUTY 					14
#define I2C_F_S 					15


/*
 * @Macro to RESET I2Cx
 */

#define I2C1_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 21) ); RCC->APB1RSTR &= ~(1 << 21);} while (0)
#define I2C2_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 22) ); RCC->APB1RSTR &= ~(1 << 22);} while (0)
#define I2C3_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 23) ); RCC->APB1RSTR &= ~(1 << 23);} while (0)




/* *********************************************************************************************** */
/* ************************************ USART DRIVER *********************************************** */

/*
 * This is USART register definition structure
 */

typedef struct
{
	__vo uint32_t SR;						/* !< USART  Status Register	  	 		   >! */
	__vo uint32_t DR;						/* !< USART  	Data Register  	 			   >! */
	__vo uint32_t BRR;						/* !< USART Baud Rate Register 	 	  	 	   >! */
	__vo uint32_t CR1;						/* !< USART Control Register 1		 	  	   >! */
	__vo uint32_t CR2;						/* !< USART Control Register 2 	 			   >! */
	__vo uint32_t CR3;						/* !< USART Control Register 3		  	 	   >! */
	__vo uint32_t GTPR;						/* !< USART Guard Time And Prescaler Register  >! */
} USART_RegDef_t;


/*
 * Macro define USARTx
 */

#define USART1					(USART_RegDef_t *) (USART1_BASEADDR)
#define USART2					(USART_RegDef_t *) (USART2_BASEADDR)
#define USART6					(USART_RegDef_t *) (USART6_BASEADDR)


/*
 * Macro for bit position for USART
 */

/*
 * @USART_SR register
 */
#define USART_SR_PE					0
#define USART_SR_FE					1
#define USART_SR_NF					2
#define USART_SR_ORE				3
#define USART_SR_IDLE				4
#define USART_SR_RXNE				5
#define USART_SR_TC					6
#define USART_SR_TXE				7
#define USART_SR_LBD				8
#define USART_SR_CTS				9

/*
 * @USART_CR1 register
 */
#define USART_CR1_SBK				0
#define USART_CR1_RWU				1
#define USART_CR1_RE				2
#define USART_CR1_TE				3
#define USART_CR1_IDLEIE			4
#define USART_CR1_RXNEIE			6
#define USART_CR1_TCIE				7
#define USART_CR1_TXEIE				8
#define USART_CR1_PEIE				9
#define USART_CR1_PS				0
#define USART_CR1_PCE				10
#define USART_CR1_WAKE				11
#define USART_CR1_M					12
#define USART_CR1_UE				13
#define USART_CR1_OVER8				15

/*
 * @USART_CR2 register
 */
#define USART_CR2_ADD0				0
#define USART_CR2_ADD1				1
#define USART_CR2_ADD2				2
#define USART_CR2_ADD3				3
#define USART_CR2_LBDL				5
#define USART_CR2_LBDIE				6
#define USART_CR2_LBCL				7
#define USART_CR2_CPHA				8
#define USART_CR2_CPOL				10
#define USART_CR2_CLKEN				11
#define USART_CR2_STOP0				12
#define USART_CR2_STOP1				13
#define USART_CR2_LINEN				14

/*
 * @USART_CR3 register
 */
#define USART_CR3_EIE				0
#define USART_CR3_IREN				1
#define USART_CR3_IRLP				2
#define USART_CR3_HDSEL				3
#define USART_CR3_NACK				4
#define USART_CR3_SCEN				5
#define USART_CR3_DMAR				6
#define USART_CR3_DMAT				7
#define USART_CR3_RTSE				8
#define USART_CR3_CTSE				9
#define USART_CR3_CTSIE				10
#define USART_CR3_ONEBIT			11


/*
 * @Macro to RESET USARTx
 */

#define USART1_REG_RESET()			do{( RCC->APB2RSTR |= (1 << 4 ) ); RCC->APB2RSTR &= ~(1 << 4 );} while (0)
#define USART2_REG_RESET()			do{( RCC->APB1RSTR |= (1 << 17) ); RCC->APB1RSTR &= ~(1 << 17);} while (0)
#define USART6_REG_RESET()			do{( RCC->APB2RSTR |= (1 << 5 ) ); RCC->APB2RSTR &= ~(1 << 5);} while (0)

























































































































































































































































































































































































































#include "stm32f411xx_spi_driver.h"
#include "stm32f411xx_i2c_driver.h"
#include "stm32f411xx_gpio_driver.h"
#include "stm32f411xx_usart_driver.h"
#include "stm32f411xx_rcc_driver.h"


#endif /* INC_STM32F411XX_H_ */
