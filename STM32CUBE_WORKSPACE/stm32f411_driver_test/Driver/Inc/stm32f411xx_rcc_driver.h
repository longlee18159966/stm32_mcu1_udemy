/* stm32f411xx_rcc_driver.h */

#ifndef RCC_DRIVER_H_
#define RCC_DRIVER_H_

#include "stm32f411xx.h"


/*
 * get RCC value from pclk1 APB1
 */
uint32_t RCC_GetPCLK1Value (void);


/*
 * get RCC value from pclk2 APB2
 */
uint32_t RCC_GetPCLK2Value (void);















#endif
