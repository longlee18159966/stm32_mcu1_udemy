/* stm32f411xx_spi_driver.c */

#include "stm32f411xx_spi_driver.h"
#include "stm32f411xx.h"

/*
 * SPI NSS pin configuration
*/

/* *************************************************************************************************
 * @fn						- SPI_NSSConfiguration
 *
 * @brief					- This function configure NSS pin
 *
 * @param[in]				- base address of the SPI register
 * @param[in]				- ENALBE or DISABLE bit
 * @param[in]				- State (INPUT or OUTPUT)
 * @return					- none
 *
 * @note					- none
 */

void SPI_NSSConfiguration (SPI_RegDef_t *pSPIx, uint8_t EnorDi, uint8_t NssPinState)
{
	if (NssPinState == INPUT)
	{
		if (EnorDi == ENABLE)
		{
			pSPIx->CR1	|= ENABLE << SPI_CR1_SSM;
			pSPIx->CR1	|= ENABLE << SPI_CR1_SSI;
		}
		else
		{
			pSPIx->CR1	&= ~(ENABLE << SPI_CR1_SSM);
			pSPIx->CR1	&= ~(ENABLE << SPI_CR1_SSI);
		}
	}
	else if (NssPinState == OUTPUT)
	{
		if (EnorDi == ENABLE)
		{
			pSPIx->CR2	|= ENABLE << SPI_CR2_SSOE;
		}
		else
		{
			pSPIx->CR2	&= ~(ENABLE << SPI_CR2_SSOE);
		}
	}
}




/*
 * SPI Enable or Disable function
*/

/* *************************************************************************************************
 * @fn						- SPI_PeripheralControl
 *
 * @brief					- This function Enable or Disable SPI peripheral
 *
 * @param[in]				- base address of the SPI register
 * @param[in]				- State (ENABLE or DISABLE)
 * @param[in]				-
 * @return					- none
 *
 * @note					- none
 */




void SPI_PeripheralControl (SPI_RegDef_t *pSPIx, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		pSPIx->CR1	|= ENABLE << SPI_CR1_SPE;
	}
	else
	{
		pSPIx->CR1	&= ~(ENABLE << SPI_CR1_SPE);
	}
}
/*
 * SPI ReceiveData
 */

/* *************************************************************************************************
 * @fn						- SPI_ReceiveData
 *
 * @brief					- This function receive data
 *
 * @param[in]				- base address of the SPI register
 * @param[in]				- Register store data
 * @param[in]				-length of data (number of byte)
 *
 * @return
 */

void SPI_ReceiveData (SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t Len)
{
	while (Len)
	{
		/* wait until Rx buffer empty */
		SPI_Polling(pSPIx, SPI_FLAG_RXNE, RESET);

		//while (SPI_GetFlagStatus(pSPIx, SPI_FLAG_TXE) == FLAG_RESET);

		/* 8-bit frame format */
		if ((pSPIx->CR1 & (1 << SPI_CR1_DFF)) == FLAG_RESET)
		{
			*pTxBuffer = pSPIx->DR;
			pTxBuffer++;
			Len--;
		}
		/* 16-bit frame format */
		else
		{
			/* read 16-bit data */
			*(uint16_t *)pTxBuffer = pSPIx->DR;
			(uint16_t *)pTxBuffer++;
			Len		  -= 2;
		}
	}
}

/*
 * SPI SendData
*/

/* *************************************************************************************************
 * @fn						- SPI_SendData
 *
 * @brief					- This function SendData
 *
 * @param[in]				- base address of the SPI register
 * @param[in]				- Register store data
 * @param[in]				- Length of data (number of byte)
 *
 * @return					- none
 *
 * @note					- none
 */

void SPI_SendData (SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t Len)
{
	while (Len)
	{
		/* wait until Tx buffer empty */
		SPI_Polling(pSPIx, SPI_FLAG_TXE, RESET);

		//while (SPI_GetFlagStatus(pSPIx, SPI_FLAG_TXE) == FLAG_RESET);

		/* 8-bit frame format */
		if ((pSPIx->CR1 & (1 << SPI_CR1_DFF)) == FLAG_RESET)
		{
			pSPIx->DR = *pTxBuffer;
			pTxBuffer++;
			Len--;
		}
		/* 16-bit frame format */
		else
		{
			/* write 16-bit data */
			pSPIx->DR = *(uint16_t *)pTxBuffer;
			(uint16_t *)pTxBuffer++;
			Len		  -= 2;
		}
	}
}

void SPI_Polling (SPI_RegDef_t *pSPIx, uint32_t FlagName, uint8_t FlagStatus)
{
	while (SPI_GetFlagStatus(pSPIx, FlagName) == FlagStatus);

	return ;
}

uint8_t SPI_GetFlagStatus (SPI_RegDef_t *pSPIx, uint32_t FlagName)
{
	if (pSPIx->SR & FlagName)
	{
		return FLAG_SET;
	}

	return FLAG_RESET;
}


/*
 * Peripheral Clock Setup
*/

/* *************************************************************************************************
 * @fn						- SPI_PeriClockControl
 *
 * @brief					- This function enables or disables peripheral clock for the given SPI peripheral
 *
 * @param[in]				- base address of the SPI peripheral
 * @param[in]				- ENABLE or DISABLE macros
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void SPI_PeriClockControl (SPI_RegDef_t *pSPIx, uint8_t EnorDi)
{

	SPI_PeripheralControl(pSPIx, ENABLE);


	if (EnorDi == ENABLE)
	{
		if (pSPIx == SPI1)
		{
			SPI1_CLK_EN();
		}
		else if (pSPIx == SPI2)
		{
			SPI2_CLK_EN();
		}
		else if (pSPIx == SPI3)
		{
			SPI3_CLK_EN();
		}
		else if (pSPIx == SPI4)
		{
			SPI4_CLK_EN();
		}
		else if (pSPIx == SPI5)
		{
			SPI5_CLK_EN();
		}
	}
	else if (EnorDi == DISABLE)
	{
		if (pSPIx == SPI1)
		{
			SPI1_CLK_DI();
		}
		else if (pSPIx == SPI2)
		{
			SPI2_CLK_DI();
		}
		else if (pSPIx == SPI3)
		{
			SPI3_CLK_DI();
		}
		else if (pSPIx == SPI4)
		{
			SPI4_CLK_DI();
		}
		else if (pSPIx == SPI5)
		{
			SPI5_CLK_DI();
		}
	}
}

/*
 * SPI init
*/

/* *************************************************************************************************
 * @fn						- SPI_Init
 *
 * @brief					- This function init SPI peripheral
 *
 * @param[in]				- base address of the SPI peripheral
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void SPI_Init (SPI_Handle_t *pSPIHandle)
{

	// first let configure the SPI_CR1 register

	uint32_t tempreg = 0;

	// enable clock SPI
	SPI_PeriClockControl(pSPIHandle->pSPIx, ENABLE);

	// 1.configure the device mode
	tempreg		|= pSPIHandle->SPIConfig.SPI_DeviceMode << SPI_CR1_MSTR;


	// 2.configure the bus config
	if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_FULL_DUPLEX)
	{
		// full duplex mode
		tempreg	&= ~(1 << SPI_CR1_BIDIMODE);
		tempreg &= ~(1 << SPI_CR1_RXONLY);
	}
	else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_HALF_DUPLEX)
	{
		// half diplex mode
		tempreg |= (1 << SPI_CR1_BIDIMODE);

	}
	else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_SIMPLEX_RXONLY)
	{
		// simple RXonly
		tempreg &= ~(1 << SPI_CR1_BIDIMODE);
		tempreg |= (1 << SPI_CR1_RXONLY);
	}

	// 3. configure clock
	tempreg		|= pSPIHandle->SPIConfig.SPI_SclkSpeed << SPI_CR1_BR;

	// 4. configure data frame (DFF)
	tempreg		|= pSPIHandle->SPIConfig.SPI_DFF << SPI_CR1_DFF;

	// 5. configure CPOL
	tempreg		|= pSPIHandle->SPIConfig.SPI_CPOL << SPI_CR1_CPOL;

	// 6. configure CPHA
	tempreg		|= pSPIHandle->SPIConfig.SPI_CPHA << SPI_CR1_CPHA;

	// 7. configure SSM
	tempreg		|= pSPIHandle->SPIConfig.SPI_SSM << SPI_CR1_SSM;

	pSPIHandle->pSPIx->CR1	= tempreg;
}


/*
 * SPI deinit
*/

/* *************************************************************************************************
 * @fn						- SPI_DeInit
 *
 * @brief					- This function deinit SPI peripheral
 *
 * @param[in]				- base address of the SPI register
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void SPI_DeInit (SPI_RegDef_t *pSPIx)
{
	if (pSPIx == SPI1)
	{
		SPI1_REG_RESET();
	}
	else if (pSPIx == SPI2)
	{
		SPI2_REG_RESET();
	}
	else if (pSPIx == SPI3)
	{
		SPI3_REG_RESET();
	}
	else if (pSPIx == SPI4)
	{
		SPI4_REG_RESET();
	}
	else if (pSPIx == SPI5)
	{
		SPI5_REG_RESET();
	}
}

/*
 * IRQ configuration and ISR handling
 */

void SPI_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ISER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ISER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ISER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ISER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}
	else
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ICER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ICER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ICER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ICER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}
}


void SPI_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority)
{
	//1. find the position of register ipr and section
	uint8_t	ipr_reg	= IRQNumber / 4;
	uint8_t ipr_section	= IRQNumber % 4;
	uint8_t shift_amount = (8 * ipr_section) + (8 - NO_PR_BIT_IMPLEMENTED);

	*(NVIC_PR_BASE_ADDRESS + ipr_reg * 4) |= (IRQPriority << shift_amount);
}


uint8_t SPI_SendDataIT (SPI_Handle_t *pSPIHandle, uint8_t *pTxBuffer, uint32_t Len)
{
	uint8_t state = pSPIHandle->TxState;
	if (state != SPI_BUSY_IN_TX)
	{
		/* 1. Save the Tx buffer address and Len information in some global variables */
		pSPIHandle->pTxBuffer	= pTxBuffer;
		pSPIHandle->TxLen		= Len;

		/* 2. Mark the SPI state as busy in transmission so that
		 *    no other code can take over same SPI peripheral until transmission is over
		 */
		pSPIHandle->TxState		= SPI_BUSY_IN_TX;

		/* 3. Enable the TXEIE control bit to get interrupt whenever TXE flag is set in SR */
		pSPIHandle->pSPIx->CR2	|= 1 << SPI_CR2_TXEIE;

		/* 4. Data transmission will be handled by the IST code (will implement later) */
	}

	return state;
}


uint8_t SPI_ReceiveDataIT (SPI_Handle_t  *pSPIHandle, uint8_t *pRxBuffer, uint32_t Len)
{
	uint8_t state = pSPIHandle->RxState;
	if (state != SPI_BUSY_IN_RX)
	{
		/* 1. Save the Rx buffer address and Len information in some global variables */
		pSPIHandle->pTxBuffer	= pRxBuffer;
		pSPIHandle->RxLen		= Len;

		/* 2. Mark the SPI state as busy in receive mode so that
		 *    no other code can take over same SPI peripheral until receive is over
		 */
		pSPIHandle->RxState		= SPI_BUSY_IN_RX;

		/* 3. Enable the RXEIE control bit to get interrupt whenever RXE flag is set in SR */
		pSPIHandle->pSPIx->CR2	|= 1 << SPI_CR2_RXEIE;

		/* 4. Data receive will be handled by the IST code (will implement later) */
	}

	return state;
}

static void spi_txe_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	/* 8-bit frame format */
	if ((pSPIHandle->pSPIx->CR1 & (1 << SPI_CR1_DFF)) == FLAG_RESET)
	{
		pSPIHandle->pSPIx->DR = *pSPIHandle->pTxBuffer;
		pSPIHandle->pTxBuffer++;
		pSPIHandle->TxLen--;
	}
	/* 16-bit frame format */
	else
	{
		/* write 16-bit data */
		pSPIHandle->pSPIx->DR  = *(uint16_t *)pSPIHandle->pTxBuffer;
		(uint16_t *)pSPIHandle->pTxBuffer++;
		pSPIHandle->TxLen		  -= 2;
	}

	if (!pSPIHandle->TxLen)
	{
		/* close the spi communication */
		/* Tx is over */
		/* this prevent interrupts from setting up of TXE flag */
		SPI_CloseTransmission(pSPIHandle);

		SPI_ApplicationEventCallback(pSPIHandle, SPI_EVENT_TX_CMPLT);

	}
}
static void spi_rxe_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	/* 8-bit frame format */
	if ((pSPIHandle->pSPIx->CR1 & (1 << SPI_CR1_DFF)) == FLAG_RESET)
	{
		*pSPIHandle->pRxBuffer = pSPIHandle->pSPIx->DR;
		pSPIHandle->pRxBuffer++;
		pSPIHandle->RxLen--;
	}
	/* 16-bit frame format */
	else
	{
		/* write 16-bit data */
		*(uint16_t *)pSPIHandle->pRxBuffer = pSPIHandle->pSPIx->DR;
		(uint16_t *)pSPIHandle->pRxBuffer++;
		pSPIHandle->RxLen		  -= 2;
	}

	if (!pSPIHandle->RxLen)
	{
		/* close the spi communication */
		/* Rx is over */
		/* this prevent interrupts from setting up of RXE flag */
		SPI_CloseReception (pSPIHandle);

		SPI_ApplicationEventCallback(pSPIHandle, SPI_EVENT_RX_CMPLT);

	}
}

static void spi_ovr_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	uint8_t temp;
	/* 1 clear the OVR flag */
	if (pSPIHandle->TxState != SPI_BUSY_IN_TX)
	{
		temp = pSPIHandle->pSPIx->DR;
		temp = pSPIHandle->pSPIx->SR;
	}
	/* 2 inform the application */
	SPI_ApplicationEventCallback(pSPIHandle, SPI_EVENT_OVR_ERR);
	(void) temp;
}


void SPI_IRQHandling (SPI_Handle_t *pHandle)
{
	uint8_t temp1, temp2;
	/* 1. check for TXE */
	temp1 = pHandle->pSPIx->SR & (1 << SPI_SR_TXE);
	temp2 = pHandle->pSPIx->CR2 & (1 << SPI_CR2_TXEIE);

	if (temp1 & temp2)
	{
		/* handler TXE */
		spi_txe_interrupt_handle(pHandle);
	}

	/* 2. check for RXE */
	temp1 = pHandle->pSPIx->SR & (1 << SPI_SR_RXEN);
	temp2 = pHandle->pSPIx->CR2 & (1 << SPI_CR2_RXEIE);
	if (temp1 & temp2)
	{
		/* handler RXE */
		spi_rxe_interrupt_handle(pHandle);
	}

	/* 3. check for OVR flag */
	temp1 = pHandle->pSPIx->SR & (1 << SPI_SR_OVR);
	temp2 = pHandle->pSPIx->CR2 & (1 << SPI_CR2_ERRIE);
	if (temp1 & temp2)
	{
		/* handler RXE */
		spi_ovr_interrupt_handle(pHandle);
	}
}


void SPI_ClearOVRFlag (SPI_RegDef_t *pSPIx)
{
	uint8_t temp;
	temp = pSPIx->DR;
	temp = pSPIx->SR;
	(void) temp;
}


void SPI_CloseTransmission (SPI_Handle_t *pSPIHandle)
{
	pSPIHandle->pSPIx->CR2 &= (1 << SPI_CR2_TXEIE);
	pSPIHandle->pTxBuffer   = NULL;
	pSPIHandle->TxLen		= 0;
	pSPIHandle->TxState     = SPI_READY;
}


void SPI_CloseReception (SPI_Handle_t *pSPIHandle)
{
	pSPIHandle->pSPIx->CR2 &= (1 << SPI_CR2_RXEIE);
	pSPIHandle->pRxBuffer   = NULL;
	pSPIHandle->RxLen		= 0;
	pSPIHandle->RxState     = SPI_READY;
}

__attribute__((weak)) void SPI_ApplicationEventCallback(SPI_Handle_t *pSPIHandle, uint8_t AppEvent)
{

}




