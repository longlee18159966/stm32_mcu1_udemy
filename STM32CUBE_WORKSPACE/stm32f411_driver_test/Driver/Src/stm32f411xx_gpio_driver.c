
#include "stm32f411xx_gpio_driver.h"
#include "stm32f411xx.h"



/*
 * Peripheral Clock Setup
*/

/* *************************************************************************************************
 * @fn						- GPIO_PeriClockControl
 *
 * @brief					- This function enables or disables peripheral clock for the given GPIO port
 *
 * @param[in]				- base address of the GPIO peripheral
 * @param[in]				- ENABLE or DISABLE macros
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void GPIO_PeriClockControl (GPIO_RegDef_t *pGPIOx, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		if (pGPIOx == GPIOA)
		{
			GPIOA_PCLK_EN();
		}
		else if (pGPIOx == GPIOB)
		{
			GPIOB_PCLK_EN();
		}
		else if (pGPIOx == GPIOC)
		{
			GPIOC_PCLK_EN();
		}
		else if (pGPIOx == GPIOD)
		{
			GPIOD_PCLK_EN();
		}
		else if (pGPIOx == GPIOE)
		{
			GPIOE_PCLK_EN();
		}
		else if (pGPIOx == GPIOH)
		{
			GPIOH_PCLK_EN();
		}

	}
	else
	{
		if (pGPIOx == GPIOA)
		{
			GPIOA_PCLK_DI();
		}
		else if (pGPIOx == GPIOB)
		{
			GPIOB_PCLK_DI();
		}
		else if (pGPIOx == GPIOC)
		{
			GPIOC_PCLK_DI();
		}
		else if (pGPIOx == GPIOD)
		{
			GPIOD_PCLK_DI();
		}
		else if (pGPIOx == GPIOE)
		{
			GPIOE_PCLK_DI();
		}
		else if (pGPIOx == GPIOH)
		{
			GPIOH_PCLK_DI();
		}

	}
}

/*
 * GPIO init - deinit
*/

/* *************************************************************************************************
 * @fn						- GPIO_Init
 *
 * @brief					- This function init GPIO
 *
 * @param[in]				- base address of GPIO and GPIO config
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void GPIO_Init (GPIO_Handle_t *pGPIOHandle)
{
	/* temp reggister */
	uint32_t temp_reg = 0;

	/* Enalbe Peripheral Clock */
	GPIO_PeriClockControl(pGPIOHandle->pGPIOx, ENABLE);


	/* 1. CONFIGURE THE MODE OF GPIO PIN */
	if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode <= GPIO_MODE_ANALOG)
	{
		/* non interrupt mode */
		/* pin moder */
		temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode << (2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber));

		/* clear position 2 bit */
		pGPIOHandle->pGPIOx->MODER &= ~(0x03 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

		pGPIOHandle->pGPIOx->MODER |= temp_reg;
		temp_reg = 0;
	}
	else
	{
		/* interrupt */
		if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_FT)
		{
			// configure interrupt mode with falling edge
			// clear RTSR corresponding
			EXTI->RTSR	&= ~(1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			// set FTSR corresponding
			EXTI->FTSR	|= (1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		}
		else if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_RT)
		{
			// configure interrupt mode with rasing edge
			// clear FTSR corresponding
			EXTI->FTSR	&= ~(1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			// set RTSR corresponding
			EXTI->RTSR	|= (1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		}
		else if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_RFT)
		{
			// configure interrupt mode with both rasing and falling edge
			// set RTSR corresponding
			EXTI->RTSR	|= (1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			// set FTSR corresponding
			EXTI->FTSR	|= (1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		}

		// configure the GPIo port selection in SYSCFG_EXTICR
		uint8_t	temp1 = pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber / 4;
		uint8_t	temp2 = pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 4;
		SYSCFG_CLK_EN();
		uint8_t portcode = GPIO_BASEADRRESS_TO_CODE(pGPIOHandle->pGPIOx);
		SYSCFG->EXTICR[temp1] |= portcode << (temp2 * 4);


		// enable the EXTI interrupt delivery using IMR
		EXTI->IMR	|= (1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

	}

	/* 2. CONFIGURE THE SPEED */
	/* speed */
	temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinSpeed << (2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber));

	/* clear position 2 bit */
	pGPIOHandle->pGPIOx->OSPEEDR &= ~(0x03 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

	pGPIOHandle->pGPIOx->OSPEEDR |= temp_reg;
	temp_reg = 0;

	/* 3. CONFIGURE THE PUPD SETTINGS */
	temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinPuPdControl << (2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber));

	/* clear position 2 bit */
	pGPIOHandle->pGPIOx->PUPDR &= ~(0x03 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

	pGPIOHandle->pGPIOx->PUPDR |= temp_reg;
	temp_reg = 0;

	/* 4. CONFIGURE THE OPTYPE */
	temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinOPType << (1 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber));

	/* clear position 2 bit */
	pGPIOHandle->pGPIOx->OTYPER &= ~(0x03 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

	pGPIOHandle->pGPIOx->OTYPER |= temp_reg;
	temp_reg = 0;

	/* 5. CONFIGURE THE ALTERNATE FUNCTIONALITY */
	if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_ALTFUNC)
	{
		if (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber / 8 == 0)
		{
			temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinAltFuncMode << (4 * (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 8)));

			/* clear position 2 bit */
			pGPIOHandle->pGPIOx->AFRL &= ~(0x0F << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

			pGPIOHandle->pGPIOx->AFRL |= temp_reg;
			temp_reg = 0;

		}
		else if (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber / 8 == 1)
		{
			temp_reg = (pGPIOHandle->GPIO_PinConfig.GPIO_PinAltFuncMode << (4 * (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 8)));

			/* clear position 2 bit */
			pGPIOHandle->pGPIOx->AFRH &= ~(0x0F << (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber - 8));

			pGPIOHandle->pGPIOx->AFRH |= temp_reg;
			temp_reg = 0;

		}

	}
}

/* *************************************************************************************************
 * @fn						- GPIO_DeInit
 *
 * @brief					- This function DeInit GPIO
 *
 * @param[in]				- base address of the GPIO peripheral
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */


void GPIO_DeInit (GPIO_RegDef_t *pGPIOx)
{
	if (pGPIOx == GPIOA)
	{
		GPIOA_REG_RESET();
	}
	else if (pGPIOx == GPIOB)
	{
		GPIOB_REG_RESET();
	}
	else if (pGPIOx == GPIOC)
	{
		GPIOC_REG_RESET();
	}
	else if (pGPIOx == GPIOD)
	{
		GPIOD_REG_RESET();
	}
	else if (pGPIOx == GPIOE)
	{
		GPIOE_REG_RESET();
	}
	else if (pGPIOx == GPIOH)
	{
		GPIOH_REG_RESET();
	}


}

/*
 * data read - write
*/

/* *************************************************************************************************
 * @fn						- GPIO_ReadFromInputPin
 *
 * @brief					- This function read input pin
 *
 * @param[in]				- base address of the GPIO peripheral
 * @param[in]				- pin number
 * @param[in]				-
 *
 * @return					- (uint8_t) value read from pin
 *
 * @note					- none
 */

uint8_t GPIO_ReadFromInputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber)
{
	return (uint8_t) ((pGPIOx->IDR >> PinNumber) & 0x00000001);
}

/* *************************************************************************************************
 * @fn						- GPIO_ReadFromInputPort
 *
 * @brief					- This function read input port
 *
 * @param[in]				- base address of the GPIO peripheral
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- (uint16_t) value read from port
 *
 * @note					- none
 */

uint16_t GPIO_ReadFromInputPort (GPIO_RegDef_t *pGPIOx)
{
	return (uint16_t) (pGPIOx->IDR);
}

/* *************************************************************************************************
 * @fn						- GPIO_PeriClockControl
 *
 * @brief					- This function enables or disables peripheral clock for the given GPIO port
 *
 * @param[in]				- base address of the GPIO peripheral
 * @param[in]				- ENABLE or DISABLE macros
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void GPIO_WriteToOutputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t Value)
{
	if (Value == 1)
	{
		pGPIOx->ODR |= 1 << PinNumber;
	}
	else if (Value == 0)
	{
		pGPIOx->ODR &= ~(1 << PinNumber);
	}
}

void GPIO_WriteToOutputPort (GPIO_RegDef_t *pGPIOx, uint16_t Value)
{
	pGPIOx->ODR = Value;
}

void GPIO_ToggleOutputPin (GPIO_RegDef_t *pGPIOx, uint8_t PinNumber)
{
	pGPIOx->ODR ^= 1 << PinNumber;
}


/*
 * IRQ configuration and ISR handling
*/

void GPIO_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ISER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ISER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ISER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ISER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}
	else
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ICER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ICER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ICER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ICER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}

}

void GPIO_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority)
{
	//1. find the position of register ipr and section
	uint8_t	ipr_reg	= IRQNumber / 4;
	uint8_t ipr_section	= IRQNumber % 4;
	uint8_t shift_amount = (8 * ipr_section) + (8 - NO_PR_BIT_IMPLEMENTED);

	*(NVIC_PR_BASE_ADDRESS + ipr_reg * 4) |= (IRQPriority << shift_amount);
}

void GPIO_IRQHandling (uint8_t PinNumber)
{
	// clear the EXTI PR register conrresponding to the pin number
	if (EXTI->PR & (1 << PinNumber))
	{
		// clear
		EXTI->PR |= 1 << PinNumber;
	}

}
