/* stm32f411xx_i2c_driver.c */

#include "stm32f411xx_i2c_driver.h"
#include "stm32f411xx_rcc_driver.h"

#define READ  1
#define WRITE 0



void I2C_GenerateStartCondition (I2C_RegDef_t *pI2Cx);
void I2C_GenerateStopCondition (I2C_RegDef_t *pI2Cx);
static void I2C__ExecuteAddressPhase (I2C_RegDef_t *pI2Cx, uint8_t SlaveAddress, uint8_t read_or_write);
static void I2C_ClearADDRFlag (I2C_Handle_t *pI2CHandle);
static void I2C_ManageAcking (I2C_RegDef_t *pI2Cx, uint8_t EnorDi);
static void I2C_MasterHandlerTXEInterrupt (I2C_Handle_t *pI2CHandle);
static void I2C_MasterHandlerRXNEInterrupt (I2C_Handle_t *pI2CHandle);

/*
 * ACK management
 */
static void I2C_ManageAcking (I2C_RegDef_t *pI2Cx, uint8_t EnorDi)
{
	if (EnorDi == I2C_ACK_ENABLE)
	{
		pI2Cx->CR1 |= (1 << I2C_CR1_ACK);
	}
	else if (EnorDi == I2C_ACK_DISABLE)
	{
		pI2Cx->CR1 &= ~(1 << I2C_CR1_ACK);
	}
}

/*
 * generate stop condition
 */
void I2C_GenerateStopCondition (I2C_RegDef_t *pI2Cx)
{
	pI2Cx->CR1	|= (1 << I2C_CR1_STOP);
}

/*
 * Disable or Enable call back event in slave mode
 */
void I2C_SlaveEnableDisableCallbackEvents (I2C_RegDef_t *pI2Cx, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		pI2Cx->CR2 |= (1 << I2C_CR2_ITEVTEN);
		pI2Cx->CR2 |= (1 << I2C_CR2_ITERREN);
		pI2Cx->CR2 |= (1 << I2C_CR2_ITBUFEN);
	}
	else
	{
		pI2Cx->CR2 &= ~(1 << I2C_CR2_ITEVTEN);
		pI2Cx->CR2 &= ~(1 << I2C_CR2_ITERREN);
		pI2Cx->CR2 &= ~(1 << I2C_CR2_ITBUFEN);
	}
}

/*
 * execute address phase
 */
static void I2C__ExecuteAddressPhase (I2C_RegDef_t *pI2Cx, uint8_t SlaveAddress, uint8_t read_or_write)
{
	SlaveAddress = (SlaveAddress << 1);
	if (read_or_write == WRITE)
	{
		pI2Cx->DR	= (SlaveAddress & (~0x01));
	}
	else if (read_or_write == READ)
	{
		pI2Cx->DR	= (SlaveAddress | 0x01);
	}
}

/*
 * generate start condition
 */
void I2C_GenerateStartCondition (I2C_RegDef_t *pI2Cx)
{
	pI2Cx->CR1	|= (1 << I2C_CR1_START);
}

/*
 * clear ADDR flag
 */
static void I2C_ClearADDRFlag (I2C_Handle_t *pI2CHandle)
{
	/* check device mode */
	if (pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_MSL))
	{
		/* master mode */
		if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX)
		{
			if (pI2CHandle->RxSize == 1)
			{
				/* disable the ack */
				I2C_ManageAcking(pI2CHandle->pI2Cx, DISABLE);
				/* clear ADDR flag (read SR1, SR2 */
				uint32_t dummy1 = pI2CHandle->pI2Cx->SR1;
				uint32_t dummy2 = pI2CHandle->pI2Cx->SR2;
				(void) dummy1;
				(void) dummy2;
			}
		}
		else
		{
			/* clear ADDR flag (read SR1, SR2 */
			uint32_t dummy1 = pI2CHandle->pI2Cx->SR1;
			uint32_t dummy2 = pI2CHandle->pI2Cx->SR2;
			(void) dummy1;
			(void) dummy2;
		}
	}
	else
	{
		/* device mode */
		/* clear ADDR flag (read SR1, SR2 */
		uint32_t dummy1 = pI2CHandle->pI2Cx->SR1;
		uint32_t dummy2 = pI2CHandle->pI2Cx->SR2;
		(void) dummy1;
		(void) dummy2;
	}
}

/*
 * I2C Enable or Disable function
*/

/* *************************************************************************************************
 * @fn						- I2C_PeripheralControl
 *
 * @brief					- This function Enable or Disable I2C peripheral
 *
 * @param[in]				- base address of the I2C register
 * @param[in]				- State (ENABLE or DISABLE)
 * @param[in]				-
 * @return					- none
 *
 * @note					- none
 */

void I2C_PeripheralControl (I2C_RegDef_t *pI2Cx, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		pI2Cx->CR1 |= ENABLE << I2C_CR1_PE;
	}
	else
	{
		pI2Cx->CR1 &= ~(ENABLE << I2C_CR1_PE);
	}
}


/*
 * Peripheral Clock Setup
*/

/* *************************************************************************************************
 * @fn						- I2C_PeriClockControl
 *
 * @brief					- This function enables or disables peripheral clock for the given I2C peripheral
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				- ENABLE or DISABLE macros
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void I2C_PeriClockControl (I2C_RegDef_t *pI2Cx, uint8_t EnorDi)
{

	I2C_PeripheralControl(pI2Cx, ENABLE);
	if (EnorDi == ENABLE)
	{
		if (pI2Cx == I2C1)
		{
			I2C1_CLK_EN();
		}
		else if (pI2Cx == I2C2)
		{
			I2C2_CLK_EN();
		}
		else if (pI2Cx == I2C3)
		{
			I2C3_CLK_EN();
		}
	}
	else if (EnorDi == DISABLE)
	{
		if (pI2Cx == I2C1)
		{
			I2C1_CLK_DI();
		}
		else if (pI2Cx == I2C2)
		{
			I2C2_CLK_DI();
		}
		else if (pI2Cx == I2C3)
		{
			I2C3_CLK_DI();
		}
	}
}

/*
 * Get I2C status
*/

/* *************************************************************************************************
 * @fn						- I2C_GetFlagStatus
 *
 * @brief					- This function get status of flag register
 *
 * @param[in]				- base address of the I2C register
 * @param[in]				- flag name
 * @param[in]				- register number (SR1 or SR2)
 *
 * @return					- flag status (set or reset)
 *
 * @note					- none
 */

uint8_t I2C_GetFlagStatus (I2C_RegDef_t *pI2Cx, uint32_t FlagName, uint8_t RegNumber)
{
	if (RegNumber == 1)
	{
		if (pI2Cx->SR1 & FlagName)
		{
			return FLAG_SET;
		}
		return FLAG_RESET;
	}
	else if (RegNumber == 2)
	{
		if (pI2Cx->SR2 & FlagName)
		{
			return FLAG_SET;
		}
		return FLAG_RESET;
	}
	return FLAG_RESET;
}


/*
 * IRQ configuration and ISR handling
 */

void I2C_IRQInterruptConfig (uint8_t IRQNumber, uint8_t EnorDi)
{
	if (EnorDi == ENABLE)
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ISER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ISER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ISER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ISER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}
	else
	{
		if (IRQNumber <= 31)
		{
			//program ISER0
			*NVIC_ICER0		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 31 && IRQNumber <= 63)
		{
			//program ISER1
			*NVIC_ICER1		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 63 && IRQNumber <= 95)
		{
			//program ISER2
			*NVIC_ICER2		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 95 && IRQNumber <= 127)
		{
			//program ISER3
			*NVIC_ICER3		|= (1 << (IRQNumber % 32));
		}
		else if (IRQNumber > 127 && IRQNumber <= 159)
		{
			//program ISER4
		}
		else if (IRQNumber > 159 && IRQNumber <= 191)
		{
			//program ISER5
		}
		else if (IRQNumber > 191 && IRQNumber <= 223)
		{
			//program ISER6
		}
		else if (IRQNumber > 223 && IRQNumber <= 255)
		{
			//programISER7
		}
	}
}


void I2C_IRQPriorityConfig (uint8_t IRQNumber, uint8_t IRQPriority)
{
	//1. find the position of register ipr and section
	uint8_t	ipr_reg	= IRQNumber / 4;
	uint8_t ipr_section	= IRQNumber % 4;
	uint8_t shift_amount = (8 * ipr_section) + (8 - NO_PR_BIT_IMPLEMENTED);

	*(NVIC_PR_BASE_ADDRESS + ipr_reg * 4) |= (IRQPriority << shift_amount);
}


/*
 * I2C init
*/

/* *************************************************************************************************
 * @fn						- I2C_Init
 *
 * @brief					- This function init I2C peripheral
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @note					- none
 */

void I2C_Init (I2C_Handle_t *pI2CHandle)
{

	/* enable clock for I2C peripheral */
	I2C_PeriClockControl(pI2CHandle->pI2Cx, ENABLE);

	uint32_t temp_var = 0;
	uint16_t ccr_value = 0;

	/*1. Configure the Mode (standard or fast) */
	/*2. Configure the speed of the serial clock (SCL) */
	/* CCR calculations */
	temp_var = 0;
	if (pI2CHandle->I2C_Config.I2C_SCLSpeed <= I2C_SCL_SPEED_SM)
	{
		/* configure sm mode (standard mode) */
		ccr_value = (RCC_GetPCLK1Value() / (2*pI2CHandle->I2C_Config.I2C_SCLSpeed));
		temp_var |= (ccr_value & 0x0FFF);
	}
	else
	{
		/* configure fm mode (fast mode) */
		temp_var |= (1 << 15);
		temp_var |= (pI2CHandle->I2C_Config.I2C_FMDutyCycle << 14);
		if (pI2CHandle->I2C_Config.I2C_FMDutyCycle == I2C_FM_DUTY_2)
		{
			ccr_value = (RCC_GetPCLK1Value() / (3*pI2CHandle->I2C_Config.I2C_SCLSpeed));
		}
		else
		{
			ccr_value = (RCC_GetPCLK1Value() / (25*pI2CHandle->I2C_Config.I2C_SCLSpeed));
		}
		temp_var |= (ccr_value & 0x0FFF);
	}
	pI2CHandle->pI2Cx->CCR  = temp_var & 0xFFF;						/* mask all bit except bit 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 => &0x3F */



	/* configure the FREQ field of CR2 */
	temp_var  = 0;
	temp_var  = RCC_GetPCLK1Value() / 1000000U;
	pI2CHandle->pI2Cx->CR2  = temp_var & 0x3F;						/* mask all bit except bit 0, 1, 2, 3, 4, 5 => &0x3F */


	/*3. Configure the device address (Applicable when device is slave) */

	/* program the device own address */
	temp_var  = 0;
	temp_var |= pI2CHandle->I2C_Config.I2C_DeviceAddress << 1;
	temp_var |= 1 << 14;
	pI2CHandle->pI2Cx->CR2  = temp_var & 0x3F;

	/*4. Enable the Acking */
	/* Configure the ACK */
	I2C_ManageAcking(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_ACKControl);

	/*5. Configure the rise time for I2C pins */
	uint8_t trise;
	if (pI2CHandle->I2C_Config.I2C_SCLSpeed <= I2C_SCL_SPEED_SM)
	{
		/* configure sm mode (standard mode) */
		trise = RCC_GetPCLK1Value() / 1000000U + 1;
	}
	else
	{
		/* configure fm mode (fast mode) */
		trise = (RCC_GetPCLK1Value() * 300) / 1000000U;
	}
	trise &= 0x3F;
	pI2CHandle->pI2Cx->TRISE = trise;

}







/*
 * I2C master send data
*/

/* *************************************************************************************************
 * @fn						- I2C_MasterSendData
 *
 * @brief					- This function send data in master mode I2C
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				- pointer point to data need to send
 * @param[in]				- length of data
 * @param[in]				- address of slave
 *
 * @return					- none
 *
 * @note					- none
 */
void I2C_MasterSendData (I2C_Handle_t *pI2CHandle, uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr)
{
	/* 1. Generate the START condition */
	I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

	/*
	 * 2. Confirm that start generation is completed by checking the SB flag in the SR1
	 * Note: Until SB is cleared SCL will be stretched (pulled to LOW)
	 */
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_SB, 1));

	/* 3. Send the address of the slave with r/nw bit set to w(0) (total 8 bits) */
	I2C__ExecuteAddressPhase(pI2CHandle->pI2Cx, SlaveAddress, WRITE);

	/* 4. Confirm that address phase is completed by checking the ADDR flag in register SR1 */
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_ADDR, 1));

	/*
	 * 5. Clear the ADDR flag according to its software sequence
	 * 	  note: until ADDR is cleared SCL will be stretched (pulled to LOW)
	 */
	I2C_ClearADDRFlag(pI2CHandle);

	/* 6. send the data until len becomes 0 */
	for (; Len > 0; --Len)
	{
		/* wait until TxE = 1 (data register DR is empty) */
		while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TxE, 1));
		pI2CHandle->pI2Cx->DR = *pTxbuffer;
		++pTxbuffer;
	}

	/*
	 * 7. When len becomes zero, wait for TXE = 1 and BTF = 1 before generating the STOP condition
	 * 	  note: TXE = 1, BTF = 1 means that both SR and DR are empty and next transmission should begin
	 * 	  when BTF = 1 SCL will be stretched (pulled to LOW)
	 */
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TxE, 1) & !I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_BTF, 1));

	/* 8. Generate STOP condition and master need not to wait for the completion of stop condition
	 * 	  note: generating STOP, automatically clears the BTF
	 */
	if (Sr == NOT_REPEATED_START)
	{
		I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
	}

}


/* *************************************************************************************************
 * @fn						- I2C_MasterRceiveData
 *
 * @brief					- This function receive data in master mode I2C
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				- pointer point to data need to send
 * @param[in]				- length of data
 * @param[in]				- address of slave
 *
 * @return					- none
 *
 * @note					- none
 */
void I2C_MasterReceiveData (I2C_Handle_t *pI2CHandle, uint8_t *pRxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr)
{
	/* 1. Generate the START condition */
	I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

	/*
	 * 2. Confirm that start generation is completed by checking the SB flag in the SR1
	 * Note: Until SB is cleared SCL will be stretched (pulled to LOW)
	 */
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_SB, 1));

	/* 3. Send the address of the slave with r/nw bit set to r(1) (total 8 bits) */
	I2C__ExecuteAddressPhase(pI2CHandle->pI2Cx, SlaveAddress, READ);

	/* 4. Confirm that address phase is completed by checking the ADDR flag in register SR1 */
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_ADDR, 1));


	/* Procedure to read only 1 byte from slave */
	if (Len == 1)
	{
		/* Disable Acking */
		I2C_ManageAcking(pI2CHandle->pI2Cx, I2C_ACK_DISABLE);

		/* Clear A=the ADDR flag */
		I2C_ClearADDRFlag(pI2CHandle);

		/* Wait until RxNE becomes 1 */
		while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_RxNE, 1) & !I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_BTF, 1));

		/* Generate STOP condition */
		if (Sr == NOT_REPEATED_START)
		{
			I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
		}
		/* Read data in to buffer */
		*pRxbuffer = pI2CHandle->pI2Cx->DR;
	}
	else if (Len > 1)
	{
		/* read the data until Len becomes zero */
		for (; Len > 0;  --Len)
		{
			/* Wait until RxNE becomes 1 */
			while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_RxNE, 1) & !I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_BTF, 1));

			/* if last 2 bytes are remaining */
			if (Len == 2)
			{
				/* clear the ack bit */
				I2C_ManageAcking(pI2CHandle->pI2Cx, I2C_ACK_DISABLE);

				/* generate STOP condition */
				if (Sr == NOT_REPEATED_START)
				{
					I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
				}
			}

			/* read data from data register to buffer */
			*pRxbuffer = pI2CHandle->pI2Cx->DR;

			/* increment the buffer address */
			++pRxbuffer;
		}
	}

	/* Re-enable Acking */
	if (pI2CHandle->I2C_Config.I2C_ACKControl == I2C_ACK_ENABLE)
	{
		I2C_ManageAcking(pI2CHandle->pI2Cx, I2C_ACK_ENABLE);
	}
}

/*
 * I2C master send data interrupt
*/

/* *************************************************************************************************
 * @fn						- I2C_MasterSendDataIT
 *
 * @brief					- This function send data in master mode I2C
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				- pointer point to data need to send
 * @param[in]				- length of data
 * @param[in]				- address of slave
 *
 * @return					- state
 *
 * @note					- none
 */

uint8_t I2C_MasterSendDataIT (I2C_Handle_t *pI2CHandle, uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr)
{
	uint8_t BusyState = pI2CHandle->TxRxState;

	if ((BusyState != I2C_BUSY_IN_TX) && (BusyState != I2C_BUSY_IN_RX))
	{
		pI2CHandle->pTxBuffer       = pTxbuffer;
		pI2CHandle->TxLen	        = Len;
		pI2CHandle->TxRxState       = I2C_BUSY_IN_TX;
		pI2CHandle->DeviceAddr      = SlaveAddress;
		pI2CHandle->StartRepeat     = Sr;

		/* generate start condition */
		I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

		/* enable ITBUFEN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITBUFEN);

		/* enable ITEVTEN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITEVTEN);

		/* enable ITERREN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITERREN);
	}

	return BusyState;
}





/* *************************************************************************************************
 * @fn						- I2C_MasterRceiveDataIT
 *
 * @brief					- This function receive data in master mode I2C with interrupt
 *
 * @param[in]				- base address of the I2C peripheral
 * @param[in]				- pointer point to data need to send
 * @param[in]				- length of data
 * @param[in]				- address of slave
 *
 * @return					- state
 *
 * @note					- none
 */
uint8_t I2C_MasterReceiveDataIT (I2C_Handle_t *pI2CHandle, uint8_t *pRxbuffer, uint32_t Len, uint8_t SlaveAddress, uint8_t Sr)
{
	uint8_t BusyState = pI2CHandle->TxRxState;

	if ((BusyState != I2C_BUSY_IN_TX) && (BusyState != I2C_BUSY_IN_RX))
	{
		pI2CHandle->pRxBuffer       = pRxbuffer;
		pI2CHandle->RxLen	        = Len;
		pI2CHandle->TxRxState       = I2C_BUSY_IN_RX;
		pI2CHandle->RxSize		    = Len;
		pI2CHandle->DeviceAddr      = SlaveAddress;
		pI2CHandle->StartRepeat     = Sr;

		/* generate start condition */
		I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

		/* enable ITBUFEN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITBUFEN);

		/* enable ITEVTEN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITEVTEN);

		/* enable ITERREN control bit */
		pI2CHandle->pI2Cx->CR2	   |= (1 << I2C_CR2_ITERREN);
	}

	return BusyState;
}

static void I2C_MasterHandlerTXEInterrupt (I2C_Handle_t *pI2CHandle)
{
	/* TxE flag is set */
	/* data register is empty */
	/* we have to do data transmission */
	if (pI2CHandle->TxRxState == I2C_BUSY_IN_TX)
	{
		if (pI2CHandle->TxLen > 0)
		{
			/* 1. load the data into DR */
			pI2CHandle->pI2Cx->DR = *(pI2CHandle->pTxBuffer);

			/* 2. decrement the TxLen */
			--(pI2CHandle->TxLen);

			/* 3. Increment the buffer address */
			++(pI2CHandle->pTxBuffer);
		}
	}
}

static void I2C_MasterHandlerRXNEInterrupt (I2C_Handle_t *pI2CHandle)
{
	/* **************************************** */
	/* RxNE flag is set */
	/* data register is full */
	/* we have to do data reception */
	if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX)
	{
		if (pI2CHandle->RxSize == 1)
		{
			*(pI2CHandle->pRxBuffer) = pI2CHandle->pI2Cx->DR;
			--(pI2CHandle->RxLen);
		}
		if (pI2CHandle->RxSize > 1)
		{
			if (pI2CHandle->RxSize == 2)
			{
				/* clear ack bit */
				I2C_ManageAcking(pI2CHandle->pI2Cx, DISABLE);

				/* read DR */
				*(pI2CHandle->pRxBuffer) = pI2CHandle->pI2Cx->DR;
				++(pI2CHandle->pRxBuffer);
				--(pI2CHandle->RxLen);
			}
		}
		if (pI2CHandle->RxLen == 0)
		{
			/* close the I2C data reception and notify the application */

			/* 1. generate the stop condition */
			I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
			/* 2. close the I2C Rx */
			I2C_CloseReceiveData(pI2CHandle);
			/* Notify the application */
			I2C_ApplicationEventCallback(pI2CHandle, I2C_EV_RX_CMPLT);
		}
	}

	/* ********************************************** */
}

void I2C_SlaveSendData (I2C_RegDef_t *pI2C, uint8_t data)
{
	pI2C->DR	= data;
}


uint8_t I2C_SlaveReceiveData (I2C_RegDef_t *pI2C)
{
	return (uint8_t) pI2C->DR;
}




/*********************************************************************
 * @fn      		  - I2C_EV_IRQHandling
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              -  Interrupt handling for different I2C events (refer SR1)

 */


void I2C_EV_IRQHandling(I2C_Handle_t *pI2CHandle)
{

	//Interrupt handling for both master and slave mode of a device

	uint32_t temp1, temp2, temp3;
	temp1 = temp2 = temp3 = 0;

	temp1 = pI2CHandle->pI2Cx->CR2 & (1 << I2C_CR2_ITEVTEN);
	temp2 = pI2CHandle->pI2Cx->CR2 & (1 << I2C_CR2_ITBUFEN);


	//1. Handle For interrupt generated by SB event
	//	Note : SB flag is only applicable in Master mode
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_SB);
	if (temp1 && temp3)
	{
		/* SB flag is set */
		/*
		 * This interrupt is generated because of SB event
		 * This block will not be executed in slave mode because for slave SB is always zero
		 * In this block lets executed the address phase
		 */
		if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX)
		{
			I2C__ExecuteAddressPhase(pI2CHandle->pI2Cx, pI2CHandle->DeviceAddr, READ);
		}
		else if (pI2CHandle->TxRxState == I2C_BUSY_IN_TX)
		{
			I2C__ExecuteAddressPhase(pI2CHandle->pI2Cx, pI2CHandle->DeviceAddr, WRITE);
		}

	}

	//2. Handle For interrupt generated by ADDR event
	//Note : When master mode : Address is sent
	//		 When Slave mode   : Address matched with own address
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_ADDR);
	if (temp1 && temp3)
	{
		/* ADDR flag is set */
		/* clear ADDR flag */
		I2C_ClearADDRFlag(pI2CHandle);
	}

	//3. Handle For interrupt generated by BTF(Byte Transfer Finished) event
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_BTF);
	if (temp1 && temp3)
	{
		/* BTF flag is set */
		if (pI2CHandle->RxSize == I2C_BUSY_IN_TX)
		{
			/* make sure that TxE also set */
			if (pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_TxE))
			{
				/* BTF = 1, TxE = 1 */
				if (pI2CHandle->TxLen == 0)
				{
					/* close the transmission */
					/* 1. generate the STOP condition */
					if (pI2CHandle->StartRepeat == NOT_REPEATED_START)
					{
						I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
					}
					/* 2. reset all member elements of the handle structure */
					I2C_CloseSendData(pI2CHandle);

					/* 3. notify the application about transmission complete */
					I2C_ApplicationEventCallback(pI2CHandle, I2C_EV_TX_CMPLT);
				}
			}
		}
		else if (pI2CHandle->RxSize == I2C_BUSY_IN_RX)
		{

		}
	}


	//4. Handle For interrupt generated by STOPF event
	// Note : Stop detection flag is applicable only slave mode . For master this flag will never be set
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_STOPF);
	if (temp1 && temp3)
	{
		/* STOPF flag is set */
		/* only apply in slave mode */
		/* we must clear STOPF flag by read SR1 and write to CR1 */
		pI2CHandle->pI2Cx->CR1	|= 0x0000;
		I2C_ApplicationEventCallback(pI2CHandle, I2C_EV_STOPF);
	}

	//5. Handle For interrupt generated by TXE event
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_TxE);
	if (temp1 && temp3 && temp2)
	{
		/* TXE flag is set */
		/* check for device in master mode */
		if (pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_MSL))
		{
			I2C_MasterHandlerTXEInterrupt(pI2CHandle);
		}
		else
		{
			/* slave */
			if (pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_TRA))
			{
				/* transmit mode */
				I2C_ApplicationEventCallback(pI2CHandle, I2C_EV_DATA_REQ);

			}
		}
	}

	//6. Handle For interrupt generated by RXNE event
	temp3 = pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR1_RxNE);
	if (temp1 && temp3 && temp2)
	{
		/* check for device in master mode */
		if (pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_MSL))
		{
			I2C_MasterHandlerRXNEInterrupt(pI2CHandle);
		}
		else
		{
			/* slave */
			if (!(pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_TRA)))
			{
				/* receive mode */
				I2C_ApplicationEventCallback(pI2CHandle, I2C_EV_DATA_RCV);
			}
		}
	}
}


/*********************************************************************
 * @fn      		  - I2C_ER_IRQHandling
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              - Complete the code also define these macros in the driver
						header file
						#define I2C_ERROR_BERR  3
						#define I2C_ERROR_ARLO  4
						#define I2C_ERROR_AF    5
						#define I2C_ERROR_OVR   6
						#define I2C_ERROR_TIMEOUT 7

 */

void I2C_ER_IRQHandling(I2C_Handle_t *pI2CHandle)
{

	uint32_t temp1,temp2;

    //Know the status of  ITERREN control bit in the CR2
	temp2 = (pI2CHandle->pI2Cx->CR2) & ( 1 << I2C_CR2_ITERREN);


/***********************Check for Bus error************************************/
	temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1<< I2C_SR1_BERR);
	if(temp1  && temp2 )
	{
		//This is Bus error

		//Implement the code to clear the buss error flag
		pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_BERR);

		//Implement the code to notify the application about the error
	   I2C_ApplicationEventCallback(pI2CHandle,I2C_ERROR_BERR);
	}

/***********************Check for arbitration lost error************************************/
	temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_ARLO );
	if(temp1  && temp2)
	{
		//This is arbitration lost error

		//Implement the code to clear the arbitration lost error flag
		pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_ARLO);

		//Implement the code to notify the application about the error
		I2C_ApplicationEventCallback(pI2CHandle,I2C_ERROR_ARLO);
	}

/***********************Check for ACK failure  error************************************/

	temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_AF);
	if(temp1  && temp2)
	{
		//This is ACK failure error

	    //Implement the code to clear the ACK failure error flag
		pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_AF);

		//Implement the code to notify the application about the error
		I2C_ApplicationEventCallback(pI2CHandle,I2C_ERROR_AF);
	}

/***********************Check for Overrun/underrun error************************************/
	temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_OVR);
	if(temp1  && temp2)
	{
		//This is Overrun/underrun

	    //Implement the code to clear the Overrun/underrun error flag
		pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_OVR);

		//Implement the code to notify the application about the error
		I2C_ApplicationEventCallback(pI2CHandle,I2C_ERROR_OVR);
	}

/***********************Check for Time out error************************************/
	temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_TIMEOUT);
	if(temp1  && temp2)
	{
		//This is Time out error

	    //Implement the code to clear the Time out error flag
		pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_TIMEOUT);

		//Implement the code to notify the application about the error
		I2C_ApplicationEventCallback(pI2CHandle,I2C_ERROR_TIMEOUT);
	}

}

void I2C_CloseSendData (I2C_Handle_t *pI2CHandle)
{
	/* implement the code to disable ITBUFEN Control bit */
	pI2CHandle->pI2Cx->CR2 &= ~(1 << I2C_CR2_ITBUFEN);

	/* implement the code to disable ITEVTEN Control bit */
	pI2CHandle->pI2Cx->CR2 &= ~(1 << I2C_CR2_ITEVTEN);

	pI2CHandle->TxRxState	= I2C_READY;
	pI2CHandle->pTxBuffer	= NULL;
	pI2CHandle->TxLen		= 0;

	if (pI2CHandle->I2C_Config.I2C_ACKControl == I2C_ACK_ENABLE)
	{
		I2C_ManageAcking(pI2CHandle->pI2Cx, ENABLE);
	}
}

void I2C_CloseReceiveData (I2C_Handle_t *pI2CHandle)
{
	/* implement the code to disable ITBUFEN Control bit */
	pI2CHandle->pI2Cx->CR2 &= ~(1 << I2C_CR2_ITBUFEN);

	/* implement the code to disable ITEVTEN Control bit */
	pI2CHandle->pI2Cx->CR2 &= ~(1 << I2C_CR2_ITEVTEN);

	pI2CHandle->TxRxState	= I2C_READY;
	pI2CHandle->pRxBuffer	= NULL;
	pI2CHandle->RxLen		= 0;
	pI2CHandle->RxSize		= 0;

	if (pI2CHandle->I2C_Config.I2C_ACKControl == I2C_ACK_ENABLE)
	{
		I2C_ManageAcking(pI2CHandle->pI2Cx, ENABLE);
	}
}


























































