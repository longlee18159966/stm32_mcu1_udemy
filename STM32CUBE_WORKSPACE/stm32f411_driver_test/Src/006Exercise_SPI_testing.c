
/*
 * 1. Test the SPI_SendData API to send tring "Hello World" and use the below configurations
 * 		1. SPI-2 Master mode
 * 		2 SCLK = max possible
 * 		3. DFF = 0 and DFF = 1
 */


/*
 * PB15 - SPI2_MOSI
 * PB14 - SPI2_MISO
 * PB13 - SPI2_SCK
 * PB12 - SPI_NSS
 * Alternate Function Mode : AF5
 */

#include <stm32f411xx_gpio_driver.h>
#include <stm32f411xx_spi_driver.h>
#include <string.h>

void SPI2_Init (void);
void SPI2_GPIOInits (void);

int main (void)
{
	char data[] = "Hello World\n";

	SPI2_GPIOInits();
	SPI2_Init();



	SPI_SendData(SPI2,(uint8_t *) data, strlen(data));
	/* check SPI busy flag */
	while (SPI_GetFlagStatus(SPI2, SPI_FLAG_BSY));

	SPI_PeripheralControl(SPI2, DISABLE);
	for (;;);

	return 0;
}

void SPI2_Init (void)
{
	SPI_Handle_t SPI2Handle;

	memset(&SPI2Handle, 0, sizeof(SPI2Handle));

	SPI2Handle.pSPIx									= SPI2;
	SPI2Handle.SPIConfig.SPI_BusConfig					= SPI_BUS_CONFIG_FULL_DUPLEX;
	SPI2Handle.SPIConfig.SPI_DeviceMode					= SPI_DEVICEMODE_MASTER;
	SPI2Handle.SPIConfig.SPI_SclkSpeed					= SPI_SCLK_SPEED_DIV_2;
	SPI2Handle.SPIConfig.SPI_DFF						= SPI_DFF_8_BIT;
	SPI2Handle.SPIConfig.SPI_CPOL						= SPI_CPOL_LOW;
	SPI2Handle.SPIConfig.SPI_CPHA						= SPI_CPHA_LOW;
	SPI2Handle.SPIConfig.SPI_SSM						= SPI_SSM_EN;


	SPI_Init(&SPI2Handle);
	SPI_NSSConfiguration(SPI2Handle.pSPIx, ENABLE, OUTPUT);
	// enable SPI peripheral
	SPI_PeripheralControl(SPI2Handle.pSPIx, ENABLE);
}

void SPI2_GPIOInits (void)
{
	GPIO_Handle_t SPIPins;

	memset(&SPIPins, 0, sizeof(SPIPins));

	//NSS
	SPIPins.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_OUTPUT;
	SPIPins.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO12;
	GPIO_Init(&SPIPins);

	SPIPins.pGPIOx 										= GPIOB;
	SPIPins.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_ALTFUNC;
	SPIPins.GPIO_PinConfig.GPIO_PinAltFuncMode			= 5;
	SPIPins.GPIO_PinConfig.GPIO_PinOPType				= GPIO_OUTPUT_TYPE_PP;
	SPIPins.GPIO_PinConfig.GPIO_PinPuPdControl			= GPIO_NO_PU_PD;
	SPIPins.GPIO_PinConfig.GPIO_PinSpeed				= GPIO_OUTPUT_SPEED_FAST;

	//SCK
	SPIPins.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO13;
	GPIO_Init(&SPIPins);


	SPIPins.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_ALTFUNC;
	//MOSI
	SPIPins.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO15;
	GPIO_Init(&SPIPins);

	//MISO
	SPIPins.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO14;
	GPIO_Init(&SPIPins);


}
