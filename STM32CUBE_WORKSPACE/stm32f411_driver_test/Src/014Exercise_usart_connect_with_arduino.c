
/*
 * Write a program to send some message over UART from STM32 board to Arduino board
 * The Arduino board will display the messsage (on Arduino serial monitor) sent from the ST board
 *
 * Baudrate: 115200 bps
 * Frame format: 1 stop bits, 8 data bits, no parity
 */



/*
 * use USART2
 * Rx pin: PA3
 * Tx pin: PA2
 */
#include "stm32f411xx_gpio_driver.h"
#include "stm32f411xx_i2c_driver.h"
#include "stm32f411xx_spi_driver.h"
#include "stm32f411xx_usart_driver.h"
#include "stm32f411xx_rcc_driver.h"
#include "stm32f411xx.h"
#include <string.h>
#include <stdio.h>

#define RxUSART2	GPIO_PIN_NO3
#define TxUSART2	GPIO_PIN_NO2

char msg[] = "Hello World";
USART_Handle_t usart2_handling;

void USART2_GPIOInit (void);

void USART2_Init (void);


int main (void)
{

	USART2_GPIOInit();
	USART2_Init();
	USART_PeripheralControl(usart2_handling.pUSARTx, ENABLE);

	USART_SendData(&usart2_handling, (uint8_t*) msg, strlen(msg));

	for (;;)
	{

	}

	return 0;
}

void USART2_Init (void)
{
	memset(&usart2_handling, 0, sizeof(usart2_handling));

	usart2_handling.pUSARTx								= USART2;
	usart2_handling.USARTConfig.USART_Mode				= USART_MODE_TXRX;
	usart2_handling.USARTConfig.USART_NumOfStopBits		= USART_1_STOP_BIT;
	usart2_handling.USARTConfig.USART_ParityControl		= USART_PARITY_DISABLE;
	usart2_handling.USARTConfig.USART_WordLength		= USART_WORDLENGTH_8_BIT;
	usart2_handling.USARTConfig.USART_HWFlowControl		= USART_HW_FLOW_CTRL_NONE;
	usart2_handling.USARTConfig.USART_Baud				= USART_STD_BAUD_115200;

	USART_Init(&usart2_handling);

}

void USART2_GPIOInit (void)
{
	GPIO_Handle_t USART2_GPIO;
	memset(&USART2_GPIO, 0, sizeof(USART2_GPIO));

	USART2_GPIO.pGPIOx									= GPIOA;
	USART2_GPIO.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_ALTFUNC;
	USART2_GPIO.GPIO_PinConfig.GPIO_PinOPType			= GPIO_OUTPUT_TYPE_PP;
	USART2_GPIO.GPIO_PinConfig.GPIO_PinSpeed			= GPIO_OUTPUT_SPEED_FAST;
	USART2_GPIO.GPIO_PinConfig.GPIO_PinPuPdControl		= GPIO_NO_PU_PD;
	USART2_GPIO.GPIO_PinConfig.GPIO_PinAltFuncMode		= 7;

	/* init for Tx */
	USART2_GPIO.GPIO_PinConfig.GPIO_PinNumber			= TxUSART2;
	GPIO_Init(&USART2_GPIO);

	/* init for Rx */
	USART2_GPIO.GPIO_PinConfig.GPIO_PinNumber			= RxUSART2;
	GPIO_Init(&USART2_GPIO);
}













































































































