
/*
 * SPI Master +(STM) and SPI Slave(Arduino) coomand & response based communication
 *
 *  when the button on the master is pressed, master sends a command to the slave
 *  and slave responds as per the command implementation
 *  1. Use SPI Full duplex mode
 *  2. ST board will be in SPI master mode and Arduino will be configured for SPI slave mode
 *  3. Use DFF = 0
 *  4. Use Hardware slave management (SSM = 0)
 *  5. SCLK speed = 2MHz, fclk = 16MHz
 */

#include <stm32f411xx_gpio_driver.h>
#include <stm32f411xx_spi_driver.h>
#include <string.h>

/*
 * PB15 - SPI2_MOSI
 * PB14 - SPI2_MISO
 * PB13 - SPI2_SCK
 * PB12 - SPI2_NSS
 */

#define COMMAND_LED_CTRL					0x50
#define COMMAND_SENSOR_READ					0x51
#define COMMAND_LED_READ					0x52
#define COMMAND_PRINT						0x53
#define COMMAND_ID_READ						0x54

#define LED_ON								0x01
#define LED_OFF								0x00

/* Arduino analog pins */
#define ANALOG_PIN0							0x00
#define ANALOG_PIN1							0x01
#define ANALOG_PIN2							0x02
#define ANALOG_PIN3							0x03
#define ANALOG_PIN4							0x04

#define LED_PIN								0x09

void delay (void)
{
	for (uint32_t i = 0; i < 25000; ++i);
}

uint8_t SPI_VerifyResponse (uint8_t ackbyte)
{
	if (ackbyte == 0xf5)
	{
		/* ack */
		return 1;
	}

	/* nack */
	return 0;
}

void GPIO_Button_Init (void);
void SPI2_GPIO_Init (void);
void SPI2_Init (void);

int main (void)
{
	GPIO_Button_Init();
	SPI2_GPIO_Init();
	SPI2_Init();
	uint8_t dummy_write = 0xff;
	uint8_t	dummy_read;
	for (;;)
	{
		/* **************************************** */


		/* 1. CMD_LED_CTRL <pin number> <value> */
		/* wait till button is pressed */
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0));
		/* to avoid button de-bouncing related issues 200ms of delay */
		delay();
		uint8_t commandcode = COMMAND_LED_CTRL;
		uint8_t ackbyte;
		uint8_t args[2];

		/* send command code */
		SPI_SendData(SPI2, &commandcode, 1);

		/* do dummy read to clear off the RXNE */
		SPI_ReceiveData(SPI2, &dummy_read, 1);

		/* send some dummy bit (1 byte) to fetch the response from slave */
		SPI_SendData(SPI2, &dummy_write, 1);

		/* read the ack byte received */
		SPI_ReceiveData(SPI2, &ackbyte, 1);

		if (SPI_VerifyResponse(ackbyte))
		{
			/* send arguments */
			args[0] = LED_PIN;
			args[1] = LED_ON;
			SPI_SendData(SPI2, args, 2);
		}


		/* **************************************** */


		/* 2. COMMAND_SENSOR_READ <analog pin number (arduino)> */
		/* wait till button is pressed */
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0));
		/* to avoid button de-bouncing related issues 200ms of delay */
		delay();
		commandcode = COMMAND_SENSOR_READ;
		/* send command code */
		SPI_SendData(SPI2, &commandcode, 1);

		/* do dummy read to clear off the RXNE */
		SPI_ReceiveData(SPI2, &dummy_read, 1);

		/* send some dummy bit (1 byte) to fetch the response from slave */
		SPI_SendData(SPI2, &dummy_write, 1);

		/* read the ack byte received */
		SPI_ReceiveData(SPI2, &ackbyte, 1);

		if (SPI_VerifyResponse(ackbyte))
		{
			/* send arguments */
			args[0] = ANALOG_PIN0;
			SPI_SendData(SPI2, args, 1);
			/* do dummy read to clear off the RXNE */
			SPI_ReceiveData(SPI2, &dummy_read, 1);

			/* insert some delay so that slave can ready with the data */
			delay();

			/* send some dummy bit (1 byte) to fetch the response from slave */
			SPI_SendData(SPI2, &dummy_write, 1);

			/* read value of the sensor */
			uint8_t sensor_value;
			SPI_ReceiveData(SPI2, &sensor_value, 1);
		}


		/* **************************************** */


		/* 3. COMMAND_LED_READ <pin number> */
		/* wait till button is pressed */
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0));
		/* to avoid button de-bouncing related issues 200ms of delay */
		delay();
		commandcode = COMMAND_LED_READ;
		/* send command code */
		SPI_SendData(SPI2, &commandcode, 1);

		/* do dummy read to clear off the RXNE */
		SPI_ReceiveData(SPI2, &dummy_read, 1);

		/* send some dummy bit (1 byte) to fetch the response from slave */
		SPI_SendData(SPI2, &dummy_write, 1);

		/* read the ack byte received */
		SPI_ReceiveData(SPI2, &ackbyte, 1);

		if (SPI_VerifyResponse(ackbyte))
		{
			/* send arguments */
			args[0] = LED_PIN;
			SPI_SendData(SPI2, args, 1);
			/* do dummy read to clear off the RXNE */
			SPI_ReceiveData(SPI2, &dummy_read, 1);

			/* insert some delay so that slave can ready with the data */
			delay();

			/* send some dummy bit (1 byte) to fetch the response from slave */
			SPI_SendData(SPI2, &dummy_write, 1);

			/* read value of the sensor */
			uint8_t led_value;
			SPI_ReceiveData(SPI2, &led_value, 1);
		}

		/* ********************************************* */


		/* 4. COMMAND_PRINT <len> <message> */
		/* wait till button is pressed */
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0));
		/* to avoid button de-bouncing related issues 200ms of delay */
		delay();
		commandcode = COMMAND_PRINT;
		/* send command code */
		SPI_SendData(SPI2, &commandcode, 1);

		/* do dummy read to clear off the RXNE */
		SPI_ReceiveData(SPI2, &dummy_read, 1);

		/* send some dummy bit (1 byte) to fetch the response from slave */
		SPI_SendData(SPI2, &dummy_write, 1);

		/* read the ack byte received */
		SPI_ReceiveData(SPI2, &ackbyte, 1);
		uint8_t message[] = "hello world";
		if (SPI_VerifyResponse(ackbyte))
		{
			args[0] = strlen((char*)message);

			/* send length of byte to display */
			SPI_SendData(SPI2, args, 1);

			/* send message */
			SPI_SendData(SPI2, message, args[0]);
		}


		/* ********************************************** */

		/* 5. COMMAND_ID_READ */
		/* wait till button is pressed */
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0));
		/* to avoid button de-bouncing related issues 200ms of delay */
		delay();
		commandcode = COMMAND_ID_READ;
		/* send command code */
		SPI_SendData(SPI2, &commandcode, 1);

		/* do dummy read to clear off the RXNE */
		SPI_ReceiveData(SPI2, &dummy_read, 1);

		/* send some dummy bit (1 byte) to fetch the response from slave */
		SPI_SendData(SPI2, &dummy_write, 1);

		/* read the ack byte received */
		SPI_ReceiveData(SPI2, &ackbyte, 1);

		if (SPI_VerifyResponse(ackbyte))
		{
			uint8_t id[11];
			uint8_t count;
			/* read 10 byte from slave */
			for (count = 0; count < 10; ++count)
			{
				/* send some dummy bit (1 byte) to fetch the response from slave */
				SPI_SendData(SPI2, &dummy_write, 1);
				/* read data from slave to id */
				SPI_ReceiveData(SPI2, &id[count], 1);
			}
			id[10] = '\0';
		}


		/* ********************************************** */


		/* confirm SPI not busy */
		while (SPI_GetFlagStatus(SPI2, SPI_FLAG_BSY));
		/* disable SPI peripheral */
		SPI_PeripheralControl(SPI2, DISABLE);

	}

}

void SPI2_Init (void)
{
	SPI_Handle_t spi2;
	memset(&spi2, 0, sizeof(spi2));
	spi2.pSPIx										= SPI2;
	spi2.SPIConfig.SPI_DeviceMode					= SPI_DEVICEMODE_MASTER;
	spi2.SPIConfig.SPI_BusConfig					= SPI_BUS_CONFIG_FULL_DUPLEX;
	spi2.SPIConfig.SPI_CPHA							= SPI_CPHA_LOW;
	spi2.SPIConfig.SPI_CPOL							= SPI_CPOL_LOW;
	spi2.SPIConfig.SPI_DFF							= SPI_DFF_8_BIT;
	spi2.SPIConfig.SPI_SSM							= SPI_SSM_DI;
	spi2.SPIConfig.SPI_SclkSpeed					= SPI_SCLK_SPEED_DIV_8;

	SPI_Init(&spi2);

	/* enable SSOE bit */
	SPI_NSSConfiguration(spi2.pSPIx, ENABLE, OUTPUT);

	/* enable peripheral clock */
	SPI_PeriClockControl(spi2.pSPIx, ENABLE);
}

void SPI2_GPIO_Init (void)
{
	GPIO_Handle_t spi2;
	memset(&spi2, 0, sizeof(spi2));
	spi2.pGPIOx										= GPIOB;
	spi2.GPIO_PinConfig.GPIO_PinAltFuncMode			= 5;
	spi2.GPIO_PinConfig.GPIO_PinOPType				= GPIO_OUTPUT_TYPE_PP;
	spi2.GPIO_PinConfig.GPIO_PinPuPdControl			= GPIO_PIN_PU;
	spi2.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_ALTFUNC;
	spi2.GPIO_PinConfig.GPIO_PinSpeed				= GPIO_OUTPUT_SPEED_FAST;

	/* NSS */
	spi2.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO12;
	GPIO_Init(&spi2);

	/* SCK */
	spi2.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO13;
	GPIO_Init(&spi2);

	/* MOSI */
	spi2.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO15;
	GPIO_Init(&spi2);

	/* MISO */
	spi2.GPIO_PinConfig.GPIO_PinNumber				= GPIO_PIN_NO14;
	GPIO_Init(&spi2);

}


/* pinA0 is input, pull-up resistor, connect to the button */

void GPIO_Button_Init (void)
{
	GPIO_Handle_t button;
	memset(&button, 0, sizeof(button));
	button.pGPIOx									= GPIOA;
	button.GPIO_PinConfig.GPIO_PinNumber			= GPIO_PIN_NO0;
	button.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_INPUT;
	button.GPIO_PinConfig.GPIO_PinPuPdControl		= GPIO_PIN_PU;
	button.GPIO_PinConfig.GPIO_PinSpeed				= GPIO_OUTPUT_SPEED_FAST;

	GPIO_Init(&button);
}
