

/*
 * I2C Master (STM32) and I2C Slave (Arduino) communication
 * when button on the master is pressed, master should send data to the Arduino slave connected
 * The data received by the Arduino will be displayed on the Arduino serial port
 */

/*
 * 1. Use I2C SCL = 100KHZ (sm)
 * 2. Use internal pull up resistors for SDA and SCL lines
 */

/*
 * I2C1 SDA line PB7
 * I2C1 SCL line PB6
 */

/*
 * step to programming:
 * 1. configure button pin as input (button connect to GPIOB0)
 * 2. configure SDA and SCL line as output, pull-up internal resistor
 * 3. init I2C1
 * 3. transmit data in master mode
 */

#include "stm32f411xx_gpio_driver.h"
#include "stm32f411xx_i2c_driver.h"
#include "stm32f411xx_spi_driver.h"
#include <string.h>
#include <stdio.h>

#define I2C_SDA_PIN GPIO_PIN_NO7
#define I2C_SCL_PIN GPIO_PIN_NO6
#define DEVICE_ADRRESS 0x61
#define SLAVE_ADDRESS  0x68

void GPIO_Button_Init (void);
void I2C_GPIO_Init (void);
void I2C_Interface_Init (void);
void delay (void);

static I2C_Handle_t pI2C_Handle;

int main (void)
{
	uint8_t some_data[] = "test I2C";
	GPIO_Button_Init();
	I2C_GPIO_Init();
	I2C_Interface_Init();

	/* enable I2C */
	I2C_PeripheralControl(pI2C_Handle.pI2Cx, ENABLE);

	/* ack = 1 after PE = 1 */
	I2C_ManageAcking(pI2C_Handle.pI2Cx, pI2C_Handle.I2C_Config.I2C_ACKControl);


	for (;;)
	{
		/* wait until buton is pressed */
		while (!GPIO_ReadFromInputPin(GPIOB, GPIO_PIN_NO0));

		/* to avoid button de-bouncing related issues 20ms of delay */
		delay();
		/* send data */
		I2C_MasterSendData(&pI2C_Handle, some_data, strlen((char*)some_data), SLAVE_ADDRESS);
	}
	for (;;);
	return 0;
}


void I2C_Interface_Init (void)
{
	memset(&pI2C_Handle, 0, sizeof(pI2C_Handle));
	pI2C_Handle.pI2Cx										= I2C1;
	pI2C_Handle.I2C_Config.I2C_SCLSpeed						= I2C_SCL_SPEED_SM;
	/* we in master mode so device address is not meaning */
	pI2C_Handle.I2C_Config.I2C_DeviceAddress				= DEVICE_ADRRESS;
	pI2C_Handle.I2C_Config.I2C_ACKControl					= I2C_ACK_ENABLE;
	I2C_Init(&pI2C_Handle);
}

void I2C_GPIO_Init (void)
{
	GPIO_Handle_t pGPIOx_I2C;
	memset(&pGPIOx_I2C, 0, sizeof(pGPIOx_I2C));
	pGPIOx_I2C.pGPIOx									= GPIOB;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_ALTFUNC;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinOPType			= GPIO_OUTPUT_TYPE_OD;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinPuPdControl		= GPIO_PIN_PU;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinSpeed				= GPIO_OUTPUT_SPEED_FAST;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinAltFuncMode		= 4;

	/* init SDA pin */
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinNumber			= I2C_SDA_PIN;
	GPIO_Init(&pGPIOx_I2C);

	/* init SCL pin */
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinNumber			= I2C_SCL_PIN;
	GPIO_Init(&pGPIOx_I2C);
}

void GPIO_Button_Init (void)
{
	GPIO_Handle_t pGPIOx_Button;
	memset(&pGPIOx_Button, 0, sizeof(pGPIOx_Button));
	pGPIOx_Button.pGPIOx								= GPIOB;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinNumber			= GPIO_PIN_NO0;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinMode			= GPIO_MODE_INPUT;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinOPType			= GPIO_OUTPUT_TYPE_PP;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinPuPdControl	= GPIO_PIN_PU;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinSpeed			= GPIO_OUTPUT_SPEED_FAST;
	GPIO_Init(&pGPIOx_Button);
}

void delay (void)
{
	for (uint32_t i = 0; i < 25000; ++i);
}





























































































































