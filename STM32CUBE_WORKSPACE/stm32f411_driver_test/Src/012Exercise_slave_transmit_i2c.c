
/*
 * I2C master (Arduino) and I2C slave (STM32) communication
 * master should read and display data from STM32 slave connected
 * First master has to get the length of the data from the slave to read
 * subsequent data from the slave
 */

/*
 * 1. Use I2C SCL = 100 KHz (sm)
 * 2. Use internal pull up resistors for SDA and SCL lines
 */

/*
 * master sends command 0x51 to read the length (1byte) of the data from the slave
 * master sends command 0x52 to read the complete data from the slave
 */

/*
 * I2C1 SDA line PB7
 * I2C1 SCL line PB6
 */

/* Remember Master is Arduino, Slave is STM32 */


#include "stm32f411xx_gpio_driver.h"
#include "stm32f411xx_i2c_driver.h"
#include "stm32f411xx_spi_driver.h"
#include <string.h>
#include <stdio.h>

#define I2C_SDA_PIN 		GPIO_PIN_NO7
#define I2C_SCL_PIN 		GPIO_PIN_NO6
#define DEVICE_ADRRESS 		0x61
#define SLAVE_ADDRESS  		0x68
#define ONEBYTECOMMAND		0x51
#define MULTIBYTECOMMAND	0x52

void GPIO_Button_Init (void);
void I2C_GPIO_Init (void);
void I2C_Interface_Init (void);
void delay (void);

static I2C_Handle_t pI2C_Handle;

/* flag variable */
uint8_t RxComplete = RESET;
uint8_t TxBuffer[] = "stm32 slave mode testing";

int main (void)
{
	GPIO_Button_Init();
	I2C_GPIO_Init();
	I2C_Interface_Init();

	/* I2C IRQ Configuration */
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_EV, ENABLE);
	I2C_IRQInterruptConfig(IRQ_NO_I2C1_ER, ENABLE);

	I2C_SlaveEnableDisableCallbackEvents(pI2C_Handle.pI2Cx, ENABLE);

	/* enable I2C */
	I2C_PeripheralControl(pI2C_Handle.pI2Cx, ENABLE);

	/* ack = 1 after PE = 1 */
	I2C_ManageAcking(pI2C_Handle.pI2Cx, pI2C_Handle.I2C_Config.I2C_ACKControl);


	for (;;);
	return 0;
}

void I2C1_EV_IRQHandler (void)
{
	I2C_EV_IRQHandling(&pI2C_Handle);
}
void I2C1_ER_IRQHandler (void)
{
	I2C_ER_IRQHandling(&pI2C_Handle);
}


uint8_t command_code;
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle, uint8_t AppEvent)
{
	static uint8_t command_code;
	static uint8_t cnt = 0;
	if (AppEvent == I2C_EV_DATA_REQ)
	{
		/* master wants some data, slave has to send it */
		if (command_code == ONEBYTECOMMAND)
		{
			I2C_SlaveSendData(pI2CHandle->pI2Cx, strlen((char*)TxBuffer));
		}
		else if (command_code == MULTIBYTECOMMAND)
		{
			/* send the contents of Tx buffer */
			I2C_SlaveSendData(pI2CHandle->pI2Cx, TxBuffer[cnt++]);
		}
	}
	else if (AppEvent == I2C_EV_DATA_RCV)
	{
		/* data is waiting for the slave to read, slave has to read it */
		command_code = I2C_SlaveReceiveData(pI2CHandle->pI2Cx);
	}
	else if (AppEvent == I2C_ERROR_AF)
	{
		/* this happen only during slave tx */
		/* master has sent the NACK, so slave should understand that master does'nt need more data */
		command_code = 0xff;
		cnt = 0;
	}
	else if (AppEvent == I2C_EV_STOPF)
	{
		/* this happen only during slave reception */
		/* master has ended the I2C communication with the slave */
	}
}



void I2C_Interface_Init (void)
{
	memset(&pI2C_Handle, 0, sizeof(pI2C_Handle));
	pI2C_Handle.pI2Cx										= I2C1;
	pI2C_Handle.I2C_Config.I2C_SCLSpeed						= I2C_SCL_SPEED_SM;
	/* we in master mode so device address is not meaning */
	pI2C_Handle.I2C_Config.I2C_DeviceAddress				= DEVICE_ADRRESS;
	pI2C_Handle.I2C_Config.I2C_ACKControl					= I2C_ACK_ENABLE;
	I2C_Init(&pI2C_Handle);
}

void I2C_GPIO_Init (void)
{
	GPIO_Handle_t pGPIOx_I2C;
	memset(&pGPIOx_I2C, 0, sizeof(pGPIOx_I2C));
	pGPIOx_I2C.pGPIOx									= GPIOB;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_ALTFUNC;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinOPType			= GPIO_OUTPUT_TYPE_OD;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinPuPdControl		= GPIO_PIN_PU;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinSpeed				= GPIO_OUTPUT_SPEED_FAST;
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinAltFuncMode		= 4;

	/* init SDA pin */
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinNumber			= I2C_SDA_PIN;
	GPIO_Init(&pGPIOx_I2C);

	/* init SCL pin */
	pGPIOx_I2C.GPIO_PinConfig.GPIO_PinNumber			= I2C_SCL_PIN;
	GPIO_Init(&pGPIOx_I2C);
}

void GPIO_Button_Init (void)
{
	GPIO_Handle_t pGPIOx_Button;
	memset(&pGPIOx_Button, 0, sizeof(pGPIOx_Button));
	pGPIOx_Button.pGPIOx								= GPIOB;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinNumber			= GPIO_PIN_NO0;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinMode			= GPIO_MODE_INPUT;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinOPType			= GPIO_OUTPUT_TYPE_PP;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinPuPdControl	= GPIO_PIN_PU;
	pGPIOx_Button.GPIO_PinConfig.GPIO_PinSpeed			= GPIO_OUTPUT_SPEED_FAST;
	GPIO_Init(&pGPIOx_Button);
}

void delay (void)
{
	for (uint32_t i = 0; i < 25000; ++i);
}


















































