
/*
 * SPI Master (STM) and SPI Slave (Arduino) communication
 * When the button on the master is pressed, master should send string of data to the Arduino
 * Slave connected. The data received by the Arduino will be displayed on the Arduino serial port.
 */

/*
 * 1. Use SPI Full duplex mode
 * 2. ST board will be in SPI master mode and Arduino will be configured for SPI slave mode
 * 3. Use DFF = 0
 * 4. Use Hardware slave management (SSM = 0)
 * 5. SCLK speed = 2MHz, fclk = 16MHz
 */

/*
 * In this excercise master is not going to receive anything for the slave.So you may not
 * configure the MISO pin
 */

/*
 * Note: Slave does not know how many bytes of data master is going to send. So master
 * first sends the number bytes info which slave is going to receive next
 */

#include <stm32f411xx_gpio_driver.h>
#include <stm32f411xx_spi_driver.h>
#include <string.h>

/*
 * PB15 - SPI2_MOSI
 * PB14 - SPI2_MISO
 * PB13 - SPI2_SCK
 * PB12 - SPI2_NSS
*/

void delay (void)
{
	for (uint32_t i = 0; i < 50000/2; ++i)
	{

	}
}
void GPIO_Button_Init (void);
void SPI2_GPIO_Init1 (void);
void SPI2_Init (void);

int main (void)
{
	char data[] = "Hello World\n";
	GPIO_Button_Init();
	SPI2_GPIO_Init1();
	SPI2_Init();
	for (;;)
	{
		while (!GPIO_ReadFromInputPin(GPIOA, GPIO_PIN_NO0))
		{
			/* to avoid button bouncing */
			delay();
			/* enable SPI peripheral */
			SPI_PeripheralControl(SPI2, ENABLE);

			/* first send data length */
			uint8_t length = strlen(data);
			SPI_SendData(SPI2, &length, 1);

			/* send data */
			SPI_SendData(SPI2,(uint8_t *) data, strlen(data));

			/* check SPI busy flag */
			while (SPI_GetFlagStatus(SPI2, SPI_FLAG_BSY));

			/* disable SPI peripheral */
			SPI_PeripheralControl(SPI2, DISABLE);
		}
	}

	return 0;
}

void GPIO_Button_Init (void)
{
	GPIO_Handle_t GPIO_Button;
	memset(&GPIO_Button, 0, sizeof(GPIO_Button));
	GPIO_Button.pGPIOx									= GPIOA;
	GPIO_Button.GPIO_PinConfig.GPIO_PinNumber			= GPIO_PIN_NO0;
	GPIO_Button.GPIO_PinConfig.GPIO_PinMode				= GPIO_MODE_INPUT;
	GPIO_Button.GPIO_PinConfig.GPIO_PinPuPdControl		= GPIO_PIN_PU;
	GPIO_Button.GPIO_PinConfig.GPIO_PinSpeed			= GPIO_OUTPUT_SPEED_FAST;

	GPIO_Init(&GPIO_Button);

}

void SPI2_Init (void)
{
	SPI_Handle_t pSPI2;
	memset(&pSPI2, 0, sizeof(pSPI2));
	pSPI2.pSPIx											= SPI2;
	pSPI2.SPIConfig.SPI_DeviceMode						= SPI_DEVICEMODE_MASTER;
	pSPI2.SPIConfig.SPI_BusConfig						= SPI_BUS_CONFIG_FULL_DUPLEX;
	pSPI2.SPIConfig.SPI_CPHA							= SPI_CPHA_LOW;
	pSPI2.SPIConfig.SPI_CPOL							= SPI_CPOL_LOW;
	pSPI2.SPIConfig.SPI_DFF								= SPI_DFF_8_BIT;
	pSPI2.SPIConfig.SPI_SclkSpeed						= SPI_SCLK_SPEED_DIV_8;
	pSPI2.SPIConfig.SPI_SSM								= SPI_SSM_DI;

	SPI_Init(&pSPI2);

	/* enable SSOE bit */
	SPI_NSSConfiguration(pSPI2.pSPIx, ENABLE, OUTPUT);

	/* enable peripheral clock */
	SPI_PeriClockControl(pSPI2.pSPIx, ENABLE);

}

void SPI2_GPIO_Init1 (void)
{
	GPIO_Handle_t pSPI2;
	memset(&pSPI2, 0, sizeof(pSPI2));
	pSPI2.pGPIOx										= GPIOB;

	/* NSS */
	/*pSPI2.GPIO_PinConfig.GPIO_PinAltFuncMode			= 5;
	pSPI2.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_INPUT;
	pSPI2.GPIO_PinConfig.GPIO_PinNumber					= GPIO_PIN_NO12;
	GPIO_Init(&pSPI2);*/

	pSPI2.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_ALTFUNC;
	pSPI2.GPIO_PinConfig.GPIO_PinOPType					= GPIO_OUTPUT_TYPE_PP;
	pSPI2.GPIO_PinConfig.GPIO_PinPuPdControl			= GPIO_PIN_PU;
	pSPI2.GPIO_PinConfig.GPIO_PinSpeed					= GPIO_OUTPUT_SPEED_FAST;
	pSPI2.GPIO_PinConfig.GPIO_PinAltFuncMode			= 5;							/* SPI alternate function */

	/* NSS */
	pSPI2.GPIO_PinConfig.GPIO_PinNumber					= GPIO_PIN_NO12;
	GPIO_Init(&pSPI2);

	pSPI2.GPIO_PinConfig.GPIO_PinMode					= GPIO_MODE_ALTFUNC;

	/* SCK */
	pSPI2.GPIO_PinConfig.GPIO_PinNumber					= GPIO_PIN_NO13;
	GPIO_Init(&pSPI2);

	/* MOSI */
	pSPI2.GPIO_PinConfig.GPIO_PinNumber					= GPIO_PIN_NO15;
	GPIO_Init(&pSPI2);



}
